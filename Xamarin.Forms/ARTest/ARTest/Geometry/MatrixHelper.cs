﻿using ARTest.Geometry;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace ARTest
{
    public static class MatrixHelper
    {
        public static float[] ToArray(this Matrix4x4 m)
        {
            return new float[]
            {
                m.M11, m.M12, m.M13, m.M14,
                m.M21, m.M22, m.M23, m.M24,
                m.M31, m.M32, m.M33, m.M34,
                m.M41, m.M42, m.M43, m.M44,
            };
        }
        public static Matrix4x4 ToMatrix(this float[] matrix)
        {
            var m = matrix;
            return new Matrix4x4(
                m[0], m[1], m[2], m[3],
                m[4], m[5], m[6], m[7],
                m[8], m[9], m[10], m[11],
                m[12], m[13], m[14], m[15]
                );
        }
        /// <summary>
        /// Assigns values from source matrix to dest.
        /// </summary>
        /// <param name="dest"></param>
        /// <param name="sourc"></param>
        public static void AssignValues(this Matrix4x4 dest, float[] source)
        {
            var d = dest;
            var s = source;
            d.M11 = s[00]; d.M12 = s[01]; d.M13 = s[02]; d.M14 = s[03];
            d.M21 = s[04]; d.M22 = s[05]; d.M23 = s[06]; d.M24 = s[07];
            d.M31 = s[08]; d.M32 = s[09]; d.M33 = s[10]; d.M34 = s[11];
            d.M41 = s[12]; d.M42 = s[13]; d.M43 = s[14]; d.M44 = s[15];
        }

        /// <summary>
        /// Assigns values from source matrix to dest.
        /// </summary>
        /// <param name="dest"></param>
        /// <param name="sourc"></param>
        public static void AssignValues(this float[] dest, Matrix4x4 source)
        {
            var d = dest;
            var s = source;
            d[00] = s.M11; d[01] = s.M12; d[02] = s.M13; d[03] = s.M14;
            d[04] = s.M21; d[05] = s.M22; d[06] = s.M23; d[07] = s.M24;
            d[08] = s.M31; d[09] = s.M32; d[10] = s.M33; d[11] = s.M34;
            d[12] = s.M41; d[13] = s.M42; d[14] = s.M43; d[15] = s.M44;
        }

        public static float[] GetIdentity()
        {
            float[] f = new float[16];
            f.SetIdentity();
            return f;
        }

        /// <summary>
        /// Sets Identity matrix for array(16).
        /// </summary>
        /// <param name="matrix"></param>
        public static void SetIdentity(this float[] matrix)
        {
            for (int i = 0; i < 16; i++)
                matrix[i] = 0;
            matrix[0] = 1;
            matrix[5] = 1;
            matrix[10] = 1;
            matrix[15] = 1;
        }
        public static void SetIdentity(this Matrix4x4 m)
        {
            var identity = GetIdentity();
            m.AssignValues(identity);
        }

        /// <summary>
        /// Sets x,y,z from vector to matrix array starting from matrixIndex.
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="matrixIndex"></param>
        /// <param name="v"></param>
        public static void SetVector(this float[] matrix, int matrixIndex, AVector3 v)
        {
            matrix[matrixIndex + 0] = v.X;
            matrix[matrixIndex + 1] = v.Y;
            matrix[matrixIndex + 2] = v.Z;
        }
        public static void SetTranslation(this float[] matrix, AVector3 v)
        {
            matrix[12] = v.X;
            matrix[13] = v.Y;
            matrix[14] = v.Z;
        }
        public static AVector3 GetTranslation(this float[] matrix)
        {
            return new AVector3(matrix[12], matrix[13], matrix[14]);
        }

        /// <summary>
        /// Returns 3 array elements starting from matrixIndex as Vector.
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="matrixIndex"></param>
        /// <returns></returns>
        public static AVector3 GetVector(this float[] matrix, int matrixIndex)
        {
            var m = matrix;
            var i = matrixIndex;
            return new AVector3(m[i] + 0, m[i + 1], m[i + 2]);
        }

        /// <summary>
        /// Extracts X axis from matrix.
        /// </summary>
        public static AVector3 GetVectorX(this Matrix4x4 m)
            => new AVector3(m.M11, m.M12, m.M13);
        /// <summary>
        /// Extracts Y axis from matrix.
        /// </summary>
        public static AVector3 GetVectorY(this Matrix4x4 m)
            => new AVector3(m.M21, m.M22, m.M23);
        /// <summary>
        /// Extracts Z axis from matrix.
        /// </summary>
        public static AVector3 GetVectorZ(this Matrix4x4 m)
            => new AVector3(m.M31, m.M32, m.M33);
        /// <summary>
        /// Extracts Translation from matrix.
        /// </summary>
        public static AVector3 GetVectorTranslation(this Matrix4x4 m)
            => new AVector3(m.M41, m.M42, m.M43);


        public static string MatrixString(Matrix4x4 m)
        {
            return m.ToArray().MatrixString();
        }
        public static string MatrixString(this float[] matrix)
        {
            Func<int, string> fun = (j) => string.Format("{0,5:0.00}", matrix[j]);
            string[] s = new string[4];
            int i = 0;
            s[0] = $"{fun(i + 0)}, {fun(i + 1)}, {fun(i + 2)}, {fun(i + 3)} ";
            i = 4;
            s[1] = $"{fun(i + 0)}, {fun(i + 1)}, {fun(i + 2)}, {fun(i + 3)} ";
            i = 8;
            s[2] = $"{fun(i + 0)}, {fun(i + 1)}, {fun(i + 2)}, {fun(i + 3)} ";
            i = 12;
            s[3] = $"{fun(i + 0)}, {fun(i + 1)}, {fun(i + 2)}, {fun(i + 3)} ";

            string text = string.Join("\n", s);

            return text;
        }
    }
}
