﻿using ARTest.Models;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace ARTest.Services
{
    /// <summary>
    /// AR can save data here.
    /// </summary>
    public class ARDataService : Notify
    {
        public Vector3 XAxis { get => _xAxis; set => OnPropertyChanged(() => _xAxis = value); }
        public Vector3 YAxis { get => _yAxis; set => OnPropertyChanged(() => _yAxis = value); }
        public Vector3 ZAxis { get => _zAxis; set => OnPropertyChanged(() => _zAxis = value); }

        public Vector2 Press { get => _press; set => OnPropertyChanged(() => _press = value); }
        public Vector3 Camera1 { get => _camera1; set => OnPropertyChanged(() => _camera1 = value); }
        public Vector3 Camera2 { get => _camera2; set => OnPropertyChanged(() => _camera2 = value); }

        public Vector3 LastPosition { get => _lastPosition; set => OnPropertyChanged(() => _lastPosition = value); }

        public string TopTag { get => _topTag; set => OnPropertyChanged(() => _topTag = value); }
        public string BottomTag { get => _bottomTag; set => OnPropertyChanged(() => _bottomTag = value); }

        public bool IsTracking { get => _isTracking; set => OnPropertyChanged(() => _isTracking = value); }
        public string TrackingReason { get; set; }

        public int Framerate { get => _frameRate; set => OnPropertyChanged(() => _frameRate = value); }
        public bool MoveAircraft { get => _moveAircraft; set => _moveAircraft = value; }
        public bool ResetAircraft { get; set; } = false;
        /// <summary>
        /// Speed percentage.
        /// </summary>
        public float Speed { get; set; } = 0.5f;
        public bool UpdateSpeed { get; set; }

        private Vector3 _xAxis = new Vector3();
        private Vector3 _yAxis = new Vector3();
        private Vector3 _zAxis = new Vector3();
        private Vector2 _press = new Vector2();
        private Vector3 _camera1 = new Vector3();
        private Vector3 _camera2 = new Vector3();
        private Vector3 _lastPosition = new Vector3();
        private bool _isTracking= false;
        private string _topTag = "";
        private string _bottomTag = "";
        private int _frameRate = 0;
        private bool _moveAircraft = false;
    }
}
