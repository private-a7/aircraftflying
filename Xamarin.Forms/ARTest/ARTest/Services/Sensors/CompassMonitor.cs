﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace ARTest.Services.Sensors
{
    public static class CompassMonitor
    {
        public static double AngleDeg { get; private set; }

        private const int steps = 3;
        private static int counter = 0;
        private static bool running = false;
        private static double[] angles = new double[steps];
        /// <summary>
        /// Starts tracking compass.
        /// <para>Wait 50ms before fetching value.</para>
        /// </summary>
        /// <returns></returns>
        public static async Task StartTracking()
        {
            try
            {
                if (running)
                    return;
                running = true;
                counter = 0;
                if (!Compass.IsMonitoring)
                    Compass.Start(SensorSpeed.UI
                        //, applyLowPassFilter:true
                        );
                Compass.ReadingChanged += Compass_ReadingChanged;
                await Task.Run(() =>
                {
                    while (counter < steps)
                    {
                        Thread.Sleep(30);
                    }
                    // Take average value.
                    //TODO If 2 values are close, and 1 value is too much different,
                    //TODO remove it.
                    //TODO (if 1 value is too much different than rest values, 
                    //TODO discard it).
                    double d1 = Math.Abs(angles[0] - angles[1]);
                    double d2 = Math.Abs(angles[1] - angles[2]);
                    double d3 = Math.Abs(angles[2] - angles[0]);
                    double d = d1;
                    double a = angles[0];
                    if (d1 <= d2 && d1 <= d3)
                        // 1 or 2
                        a = angles[0];
                    else if (d2 <= d1 && d2 <= d3)
                        // 2 or 3
                        a = angles[1];
                    else
                        a = angles[2];

                    AngleDeg = a;
                });
                Compass.ReadingChanged -= Compass_ReadingChanged;
                //Compass.Stop();
                running = false;
            }
            catch (Exception)
            {
                Compass.ReadingChanged -= Compass_ReadingChanged;
            }
        }

        private static void Compass_ReadingChanged(object sender, CompassChangedEventArgs e)
        {
            double angleDeg = e.Reading.HeadingMagneticNorth;
            if (counter >= steps)
                return;
            angles[counter++] = angleDeg;
        }
    }
}
