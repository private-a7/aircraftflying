﻿using ARTest.Geometry;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace ARTest.Services.Sensors
{
    public static class OrientationMonitor
    {
        /// <summary>
        /// Multiply radians with this to get degrees.
        /// </summary>
        private static double radFactor = 180.0 / Math.PI;
        public static string Reading { get; private set; } = "";
        /// <summary>
        /// Tilt angle relative to Portrait mode. Positive is clockwise roll.
        /// <para>Also works if phone is Pitched.</para>
        /// </summary>
        /// <remarks>
        /// Tilt angle is calculated as angle between X axis of phone and XY plane of world 
        /// (plane is parallel to world, Z is up in world).
        /// Angle 0 means that z coordinate (in world) of x axis is 0.
        /// </remarks>
        public static float BankAngleDeg { get; private set; }
        public static float BankAngleRad { get; private set; }

        private const int steps = 3;
        private static int counter = 0;
        private static bool running = false;
        private static double[] angles = new double[steps];
        /// <summary>
        /// Starts tracking compass.
        /// <para>Wait 50ms before fetching value.</para>
        /// </summary>
        /// <returns></returns>
        public static async Task StartTracking()
        {
            try
            {
                if (running)
                    return;
                running = true;
                counter = 0;
                if (!OrientationSensor.IsMonitoring)
                    OrientationSensor.Start(SensorSpeed.UI
                        //, applyLowPassFilter:true
                        );
                OrientationSensor.ReadingChanged += OrientationSensor_ReadingChanged;
                
                //OrientationSensor.ReadingChanged -= OrientationSensor_ReadingChanged;
                //Compass.Stop();
                running = false;
            }
            catch (Exception)
            {
                OrientationSensor.ReadingChanged -= OrientationSensor_ReadingChanged;
            }
        }

        private static void OrientationSensor_ReadingChanged(object sender, OrientationSensorChangedEventArgs e)
        {
            // Orientation is coordinate system of openGL in World coordinate system (WCS).
            // World coordinate system is: x - to East, y - to North, z - up;
            // Example: 
            //      Pointing phone to North (in Portrait mode), with Z axis parallel to ground,
            //      coordinate system of OpenGL will have following coordinates:
            //      (Matrix is expressed in WCS - it shows OpenGL coordinate system in WCS)
            //      x: 1,  0,  0
            //      y: 0,  0,  1 (WCS z axis is Up, OpenGL that is y axis)
            //      z: 0, -1,  0 (WCS y axis is parallel to ground, OpenGL that is z axis)

            
            var q = e.Reading.Orientation;
            var m = Matrix4x4.CreateFromQuaternion(q);
            // Check angle of X axis.
            // If height coordinate is 0 (z), no tilt.
            if (Math.Abs(m.M13) < 0.01f)
                BankAngleDeg = 0;
            else
            {
                AVector3 v = m.GetVectorX();
                // Angle between this vector and Up axis.

                // Transforming to 2d space (to calculate angle between vectors).
                // Component of vector in XY plane.
                double l = Math.Sqrt(v.X * v.X + v.Y * v.Y);
                
                double d = v.Length;
                double alphaRad = Math.Acos(l / d);
                alphaRad *= -Math.Sign(v.Z);
                float alphaDeg = (float)(radFactor * alphaRad);
                BankAngleDeg = alphaDeg;
                BankAngleRad = (float)alphaRad;

                Reading = BankAngleDeg.ToString("0.00");
            }
            //Func<float, string> str = (d) => d.ToString("0.00");
            //string s = $"x={str(q.X)}, y={str(q.Y)}, z={str(q.Z)}, w={str(q.W)}";
            //AVector3 v;
            //v = new AVector3(m.M11, m.M12, m.M13);
            //s += $"\n x vector: {v}";
            //v = new AVector3(m.M21, m.M22, m.M23);
            //s += $"\n y vector: {v}";
            //v = new AVector3(m.M31, m.M32, m.M33);
            //s += $"\n z vector: {v}";
            //Reading = s;
        }

        private static void Compass_ReadingChanged(object sender, CompassChangedEventArgs e)
        {
            double angleDeg = e.Reading.HeadingMagneticNorth;
            if (counter >= steps)
                return;
            angles[counter++] = angleDeg;
        }
    }
}
