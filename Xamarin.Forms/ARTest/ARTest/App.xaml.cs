﻿using ARTest.Services;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ARTest
{
    public partial class App : Application
    {

        public static WindsorContainer IoC { get; } = new WindsorContainer();
        /// <summary>
        /// Don't use this from code.
        /// </summary>
        public App()
        {

            InitializeComponent();

            MainPage = new MainPage();
        }
        /// <summary>
        /// Alternative way, to start from project (with IoC initialized).
        /// </summary>
        /// <returns></returns>
        public static App Create()
        {
            var a = new App();
            IoC.Register(Component.For<ARDataService>());
            // Done here because XAML preview complains about IoC in ctor.
            a.MainPage.BindingContext = new MainPageModel();
            return a;
        }
        public async Task CheckPermissions()
        {
            await ((MainPage)MainPage).CheckPermissions();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
