﻿using ARTest.Models;
using ARTest.Services;
using Castle.Windsor.Diagnostics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;

namespace ARTest
{
    public class MainPageModel : Notify
    {
        /// <summary>
        /// Drawing per second.
        /// </summary>
        public int Framerate { get; private set; }

        public string XAxis { get => _xAxis; set => OnPropertyChanged(() => _xAxis = value); }
        public string YAxis { get => _yAxis; set => OnPropertyChanged(() => _yAxis = value); }
        public string ZAxis { get => _zAxis; set => OnPropertyChanged(() => _zAxis = value); }
        public string TopTag => data.TopTag;
        public string BottomTag => data.BottomTag;

        public string LastX { get => _lastX; set => OnPropertyChanged(() => _lastX = value); }
        public string LastY { get => _lastY; set => OnPropertyChanged(() => _lastY = value); }
        public string LastZ { get => _lastZ; set => OnPropertyChanged(() => _lastZ = value); }

        public string Camera1X { get => _camera1X; set => OnPropertyChanged(() => _camera1X = value); }
        public string Camera1Y { get => _camera1Y; set => OnPropertyChanged(() => _camera1Y = value); }
        public string Camera1Z { get => _camera1Z; set => OnPropertyChanged(() => _camera1Z = value); }
        public string Camera2X { get => _camera2X; set => OnPropertyChanged(() => _camera2X = value); }
        public string Camera2Y { get => _camera2Y; set => OnPropertyChanged(() => _camera2Y = value); }
        public string Camera2Z { get => _camera2Z; set => OnPropertyChanged(() => _camera2Z = value); }

        public string PressX { get => _pressX; set => OnPropertyChanged(() => _pressX = value); }
        public string PressY { get => _pressY; set => OnPropertyChanged(() => _pressY = value); }
        public float SpeedPercent { get => _speedPercent; set 
            {
                OnPropertyChanged(() => _speedPercent = value);
                data.UpdateSpeed = true;
                data.Speed = value / 100f;
            } }

        public bool IsTracking { get; set; }
        public string TrackingReason { get; private set; }

        public bool MoveAircraft
        {
            get => _moveAircraft; 
            set => OnPropertyChanged(() =>
                {
                    _moveAircraft = value;
                    data.MoveAircraft = value;
                });
        }


        private string _xAxis = "NOT INITIALIZED";
        private string _yAxis = "NOT INITIALIZED";
        private string _zAxis = "NOT INITIALIZED";
        private string _pressX = "";
        private string _pressY = "";
        private string _lastX = "";
        private string _lastY = "";
        private string _lastZ = "";
        private string _camera1X = "";
        private string _camera1Y = "";
        private string _camera1Z = "";
        private string _camera2X = "";
        private string _camera2Y = "";
        private string _camera2Z = "";
        private float _speedPercent = 50;

        private bool _moveAircraft = true;
        
        private ARDataService data;
        private Stopwatch sw = Stopwatch.StartNew();
        public MainPageModel()
        {
            data = App.IoC.Resolve<ARDataService>();
            MoveAircraft = true;
            SpeedPercent = data.Speed * 100;
            data.PropertyChanged += Data_PropertyChanged;
        }
        public void Reset()
        {
            data.ResetAircraft = true;
        }
        private void Data_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var a = data.XAxis;
            Func<double, string> f = (d) => d.ToString("0.000", CultureInfo.InvariantCulture);
            string s = $"X axis ({f(a.X)}, {f(a.Y)}, {f(a.Z)})";
            XAxis = s;

            a = data.YAxis;
            s = $"Y axis ({f(a.X)}, {f(a.Y)}, {f(a.Z)})";
            YAxis = s;

            a = data.ZAxis;
            s = $"Z axis ({f(a.X)}, {f(a.Y)}, {f(a.Z)})";
            ZAxis = s;

            OnPropertyChanged(nameof(TopTag));
            OnPropertyChanged(nameof(BottomTag));

            s = $"Press X: {f(data.Press.X)}";
            PressX = s;

            s = $"Press Y: {f(data.Press.Y)}";
            PressY = s;

            double delta;

            s = $"Camera1 X: {f(data.Camera1.X)}";
            Camera1X = s;
            s = $"Camera1 Y: {f(data.Camera1.Y)}";
            Camera1Y = s;
            s = $"Camera1 Z: {f(data.Camera1.Z)}";
            Camera1Z= s;
            delta = data.Camera1.X - data.Camera2.X;
            s = $"Camera2 X: {f(data.Camera2.X)}";
            Camera2X = s;
            delta = data.Camera1.Y - data.Camera2.Y;
            s = $"Camera2 Y: {f(data.Camera2.Y)}";
            Camera2Y = s;
            delta = data.Camera1.Z - data.Camera2.Z;
            s = $"Camera2 Z: {f(data.Camera2.Z)}";
            Camera2Z= s;

            Framerate = data.Framerate;
            OnPropertyChanged(nameof(Framerate));

            if (IsTracking != data.IsTracking)
            {
                IsTracking = data.IsTracking;
                TrackingReason = "";
            }
            else
            {
                TrackingReason = data.TrackingReason;
            }
            //if (sw.ElapsedMilliseconds > 100)
            {
                sw.Restart();
                OnPropertyChanged(nameof(IsTracking));
                OnPropertyChanged(nameof(TrackingReason));
            }
            MoveAircraft = data.MoveAircraft;
        }
    }
}
