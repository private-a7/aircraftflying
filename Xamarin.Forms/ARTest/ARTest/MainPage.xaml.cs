﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using static Xamarin.Essentials.Permissions;

namespace ARTest
{
    public partial class MainPage : ContentPage
    {

        public MainPage()
        {
            InitializeComponent();
        }
        public async Task CheckPermissions()
        {
            await checkPermissionsInternal();
        }
        private async Task checkPermissionsInternal()
        {
            string msg;
            bool access;

            msg = "Location permission not granted!";
            access = await checkSinglePermission<Permissions.LocationWhenInUse>(msg);
            if (!access)
                return;

            msg = "Camera permission not granted!";
            access = await checkSinglePermission<Permissions.Camera>(msg);
            if (!access)
                return;

            msg = "Storage permission not granted!";
            access = await checkSinglePermission<Permissions.StorageWrite>(msg);
        }
        private async Task<bool> checkSinglePermission<T>(string errorMsg) where T: BasePlatformPermission, new()
        {
            var status = await Permissions.CheckStatusAsync<T>();
            if (status != PermissionStatus.Granted)
            {
                status = await Permissions.RequestAsync<T>();
                if (status != PermissionStatus.Granted)
                {
                    await DisplayAlert("Error", errorMsg, "Ok");
                    return false;
                }
            }
            return true;
        }

        private void reset_Click(object sender, EventArgs e)
        {
            ((MainPageModel)BindingContext).Reset();
        }
    }
}
