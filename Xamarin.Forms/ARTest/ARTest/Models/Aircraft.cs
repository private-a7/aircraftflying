﻿using ARTest.Geometry;
using Castle.MicroKernel.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;

namespace ARTest.Models
{

    /// <summary>
    /// Properties of aircraft model.
    /// </summary>
    /// <remarks>
    /// Aircraft model which is loaded must be oriented:
    /// - front towards -z axis
    /// - up is y axis
    /// </remarks>
    public class Aircraft
    {

        public const float MaxBankAngle = (float)(Math.PI / 4);
        public const float ZeroPrecisionRad = (float)(Math.PI / 180);
        public const float MaxSpeed = 2f;

        /// <summary>
        /// Length of cylinder body of aircraft (size of model, no scaled).
        /// </summary>
        public float ModelLength { get; set; } = 9;
        /// <summary>
        /// Width of cylinder body of aircraft (size of model, no scaled).
        /// </summary>
        public float ModelWidth { get; set; } = 1;

        /// <summary>
        /// Scale of model to be displayed.
        /// </summary>
        public float ModelScale { get; set; } = 0.4f;

        /// <summary>
        /// Transform matrix of current Aircraft.
        /// <para>Used to be drawn.</para>
        /// </summary>
        public float[] Transform { get; private set; }

        public AVector3 Direction => direction;
        public AVector3 Position { get; private set; } = new AVector3();

        /// <summary>
        /// Speed expressed as m/s.
        /// </summary>
        public float Speed
        {
            get => _speed; set
            {
                _speed = value;
                MoveStep = _speed / 30;
                if (TurnRadius > 0.0001f)
                {
                    var deg = (float)(180 * MoveStep / (TurnRadius * Math.PI));
                    MoveStepAngleRad = (float)(deg * Math.PI / 180);
                }
                else
                    MoveStepAngleRad = 0;
            }
        }

        /// <summary>
        /// How much to move each step.
        /// </summary>
        public float MoveStep { get; private set; } = 0.01f;
        /// <summary>
        /// Used when turning = MoveStep on circle with current radius.
        /// </summary>
        public float MoveStepAngleRad { get; private set; }


        public float RollAngle { get; private set; }

        private float TurnRadius { get; set; }
        private AVector3 direction { get; set; }
        /// <summary>
        /// Interchange 2 arrays, to save performance.
        /// </summary>
        private float[] backup { get; set; } = new float[16];
        private float _speed;
        public Aircraft()
        {
            //Speed = 0.8f;
            Transform = MatrixHelper.GetIdentity();
            SetDirection(new AVector3(0, 0, -1));
        }

        /// <summary>
        /// Resets orientation of aircraft, with optional position.
        /// </summary>
        /// <param name="position"></param>
        public void Reset(AVector3? position = null)
        {
            if (position.HasValue)
                Position = position.Value;
            else
                Position = new AVector3(0, 0, 0);
            Transform = MatrixHelper.GetIdentity();
            SetDirection(new AVector3(0, 0, -1));
        }

        /// <summary>
        /// Sets direction of aircraft with Roll (Pitch and Yaw are calculated from vector).
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="rollRad">Roll of aircaft (clockwise rotation along vector).</param>
        public void SetDirection(AVector3 vector, float rollRad)
        {
            SetDirection(vector);

            SetRoll(rollRad);
        }
        /// <summary>
        /// Sets direction of aircraft (Roll is 0, Pitch and Yaw are inherited from direction).
        /// </summary>
        public void SetDirection(AVector3 vector)
        {
            vector = vector.Negate();
            setDirection(backup, vector);
            backup.SetTranslation(Position);
            interchangeTransform();
        }

        public void SetRoll(float angleRad)
        {
            float rollRad = angleRad;// (float)(rollDeg * Math.PI / 180);
            if (rollRad < -MaxBankAngle)
                rollRad = -MaxBankAngle;
            else if (rollRad > MaxBankAngle)
                rollRad = MaxBankAngle;
            RollAngle = rollRad;
            if (Math.Abs(rollRad) < ZeroPrecisionRad)
                TurnRadius = 0;
            else
            {
                float r = (float)(rollRad / MaxBankAngle);
                float minRadius = 1;
                r = minRadius + (1 - Math.Abs(r)) * 10;
                TurnRadius = r;
            }
            var rotM = Matrix4x4.CreateRotationZ(rollRad);
            setDirection(backup, direction);
            var res = Matrix4x4.Multiply(rotM, backup.ToMatrix());
            backup = res.ToArray();
            backup.SetTranslation(Position);
            interchangeTransform();
            Speed = Speed;
        }
        private void setDirection(float[] matrix, AVector3 v)
        {
            var vector = v;
            var right = vector * AVector3.YAxis;
            var up = right * vector;
            matrix.SetIdentity();
            matrix.SetVector(0, right);
            matrix.SetVector(4, up);
            matrix.SetVector(8, vector);
            matrix.SetTranslation(Position);
            direction = vector.GetUnit();
        }
        private void interchangeTransform()
        {
            var v = Transform;
            Transform = backup;
            backup = v;
        }


        public void SetPosition(AVector3 v)
        {
            Position = v;
            Transform.SetVector(12, v);
        }
        /// <summary>
        /// Manually set transform matrix.
        /// </summary>
        public void SetTransform(float[] matrix)
        {
            for (int i = 0; i < 16; i++)
                Transform[i] = matrix[i];
            Position = matrix.GetTranslation();
        }
        /// <summary>
        /// Manually set transform matrix.
        /// </summary>
        public void SetTransform(Matrix4x4 matrix)
        {
            Transform.AssignValues(matrix);
            Position = Transform.GetTranslation();
        }

        /// <summary>
        /// Move aircraft by single step.
        /// </summary>
        public void Move()
        {
            if (TurnRadius < 0.0001f)
            {
                var pos = Position + direction.Negate() * MoveStep;
                SetPosition(pos);
            }
            else
            {
                int side = RollAngle < 0 ? -1 : 1;
                var ortho = direction * AVector3.YAxis;
                if (RollAngle < 0)
                    ortho = ortho.Negate();
                ortho = ortho.GetUnit() * TurnRadius;
                var offset = ortho + Position.Negate();

                var yaw = Matrix4x4.CreateRotationY(-side * MoveStepAngleRad);
                var v1 = Vector3.Transform(ortho, yaw);
                var position = offset.Negate() + v1;
                SetPosition(position);                

                var v = Vector3.Transform(direction, yaw);
                SetDirection(v.ToAVector3().Negate(), RollAngle);
                 

            }
        }
    }
}
