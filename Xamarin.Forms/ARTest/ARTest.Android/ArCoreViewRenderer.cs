﻿using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using ARTest.Droid.Models.OpenGLs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms.Platform.Android.FastRenderers;

[assembly: ExportRenderer(typeof(ARTest.ARView), typeof(ARTest.Droid.ArCoreViewRenderer))]
namespace ARTest.Droid
{
    public class ArCoreViewRenderer : FrameLayout, IVisualElementRenderer, IViewRenderer
    {
        /*
        public VisualElementTracker Tracker { get; }

        public ViewGroup ViewGroup { get; }
        public Android.Views.View View => this;
        public ARCoreView Element
        {
            get => _element;
            set
            {
                if (_element == value)
                    return;
                var oldElement = _element;
                _element = value;
                OnElementChanged(new ElementChangedEventArgs<ARCoreView>(oldElement, _element));
            }
        }

        public event EventHandler<VisualElementChangedEventArgs> ElementChanged;
        public event EventHandler<PropertyChangedEventArgs> ElementPropertyChanged;


        private VisualElementRenderer visualElementRenderer;
        private ArFragment arFragment;
        private FragmentManager FManager => Context.GetFragmentManager();
        private ARCoreView _element;
        private bool disposed = false;
        public ArCoreViewRenderer(Context context) : base(context)
        {
            visualElementRenderer = new VisualElementRenderer(this);
        }

        private void OnElementChanged(ElementChangedEventArgs<ARCoreView> e)
        {
            ArFragment newFragment = null;

            if (e.OldElement != null)
            {
                e.OldElement.PropertyChanged -= OnElementPropertyChanged;
                arFragment.Dispose();
            }
            if (e.NewElement != null)
            {
                this.EnsureId();

                e.NewElement.PropertyChanged += OnElementPropertyChanged;

                ElevationHelper.SetElevation(this, e.NewElement);
                newFragment = new ArFragment { Element = element };
            }

            FManager.BeginTransaction()
                .Replace(Id, arFragment = newFragment, "camera")
                .Commit();
            ElementChanged?.Invoke(this, new VisualElementChangedEventArgs(e.OldElement, e.NewElement));
        }
        async void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            ElementPropertyChanged?.Invoke(this, e);

            switch (e.PropertyName)
            {
                case "Width":
                    //await cameraFragment.RetrieveCameraDevice();
                    break;
            }
        }
        public SizeRequest GetDesiredSize(int widthConstraint, int heightConstraint)
        {
            throw new NotImplementedException();
        }

        public void MeasureExactly()
        {
            throw new NotImplementedException();
        }

        public void SetElement(VisualElement element)
        {
            throw new NotImplementedException();
        }

        public void SetLabelFor(int? id)
        {
            throw new NotImplementedException();
        }

        public void UpdateLayout()
        {
            throw new NotImplementedException();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposed)
                return;
            disposed = true;
            arFragment.Dispose();
            if (disposing)
            {
                Tracker?.Dispose();
                Tracker = null;
            }
        }*/

        public event EventHandler<VisualElementChangedEventArgs> ElementChanged;
        public event EventHandler<PropertyChangedEventArgs> ElementPropertyChanged;




        VisualElement IVisualElementRenderer.Element => Element;
        VisualElementTracker IVisualElementRenderer.Tracker => visualElementTracker;
        ViewGroup IVisualElementRenderer.ViewGroup => null;
        Android.Views.View IVisualElementRenderer.View => this;

        int? defaultLabelFor;
        bool disposed;
        ARView element;
        VisualElementTracker visualElementTracker;
        VisualElementRenderer visualElementRenderer;
        FragmentManager fragmentManager;
        GLFragment arFragment;

        FragmentManager FragmentManager { get { fragmentManager = Context.GetFragmentManager(); return fragmentManager; } }


        ARView Element
        {
            get => element;
            set
            {
                if (element == value)
                {
                    return;
                }

                var oldElement = element;
                element = value;
                OnElementChanged(new ElementChangedEventArgs<ARView>(oldElement, element));
            }
        }

        public ArCoreViewRenderer(Context context) : base(context)
        {
            visualElementRenderer = new VisualElementRenderer(this);
        }

        void OnElementChanged(ElementChangedEventArgs<ARView> e)
        {
            GLFragment newFragment = null;

            if (e.OldElement != null)
            {
                e.OldElement.PropertyChanged -= OnElementPropertyChanged;
                arFragment.Dispose();
            }
            if (e.NewElement != null)
            {
                this.EnsureId();

                e.NewElement.PropertyChanged += OnElementPropertyChanged;

                ElevationHelper.SetElevation(this, e.NewElement);
                newFragment = new GLFragment();
            }
            FragmentManager.BeginTransaction()
                .Replace(Id, arFragment = newFragment, "ar_fragment")
                .Commit();
            ElementChanged?.Invoke(this, new VisualElementChangedEventArgs(e.OldElement, e.NewElement));
        }

        void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            ElementPropertyChanged?.Invoke(this, e);

            switch (e.PropertyName)
            {
                case "Width":
                    //await cameraFragment.RetrieveCameraDevice();
                    break;
            }
        }

        void IViewRenderer.MeasureExactly() => MeasureExactly(this, Element, Context);

        static void MeasureExactly(Android.Views.View control, VisualElement element, Context context)
        {
            if (control == null || element == null)
            {
                return;
            }

            double width = element.Width;
            double height = element.Height;

            if (width <= 0 || height <= 0)
            {
                return;
            }

            int realWidth = (int)context.ToPixels(width);
            int realHeight = (int)context.ToPixels(height);

            int widthMeasureSpec = MeasureSpecFactory.MakeMeasureSpec(realWidth, MeasureSpecMode.Exactly);
            int heightMeasureSpec = MeasureSpecFactory.MakeMeasureSpec(realHeight, MeasureSpecMode.Exactly);

            control.Measure(widthMeasureSpec, heightMeasureSpec);
        }

        SizeRequest IVisualElementRenderer.GetDesiredSize(int widthConstraint, int heightConstraint)
        {
            Measure(widthConstraint, heightConstraint);
            SizeRequest result = new SizeRequest(new Size(MeasuredWidth, MeasuredHeight), new Size(Context.ToPixels(20), Context.ToPixels(20)));
            return result;
        }

        void IVisualElementRenderer.SetElement(VisualElement element)
        {
            if (!(element is ARView camera))
            {
                throw new ArgumentException($"{nameof(element)} must be of type {nameof(ARView)}");
            }

            if (visualElementTracker == null)
            {
                visualElementTracker = new VisualElementTracker(this);
            }
            Element = camera;
        }

        void IVisualElementRenderer.SetLabelFor(int? id)
        {
            if (defaultLabelFor == null)
            {
                defaultLabelFor = LabelFor;
            }
            LabelFor = (int)(id ?? defaultLabelFor);
        }

        void IVisualElementRenderer.UpdateLayout() => visualElementTracker?.UpdateLayout();


        static class MeasureSpecFactory
        {
            public static int GetSize(int measureSpec)
            {
                const int modeMask = 0x3 << 30;
                return measureSpec & ~modeMask;
            }

            public static int MakeMeasureSpec(int size, MeasureSpecMode mode) => size + (int)mode;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            arFragment.Dispose();
            disposed = true;

            if (disposing)
            {
                SetOnClickListener(null);
                SetOnTouchListener(null);

                if (visualElementTracker != null)
                {
                    visualElementTracker.Dispose();
                    visualElementTracker = null;
                }

                if (visualElementRenderer != null)
                {
                    visualElementRenderer.Dispose();
                    visualElementRenderer = null;
                }

                if (Element != null)
                {
                    Element.PropertyChanged -= OnElementPropertyChanged;

                    if (Platform.GetRenderer(Element) == this)
                    {
                        Platform.SetRenderer(Element, null);
                    }
                }
            }

            base.Dispose(disposing);
        }
    }
}