﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ARTest.Droid.Models.OpenGLs
{
    public class TouchListener : Java.Lang.Object, Android.Views.View.IOnTouchListener
    {
        public Func<View, MotionEvent, bool> Touched;
        public bool OnTouch(View v, MotionEvent e)
        {
            return Touched?.Invoke(v, e) ?? false;
        }
    }
}