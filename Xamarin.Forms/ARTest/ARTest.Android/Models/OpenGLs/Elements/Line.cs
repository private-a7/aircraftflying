﻿using Android.App;
using Android.Content;
using Android.Opengl;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Nio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Android.Opengl.GLES30;
using static Android.Opengl.GLES20;
using System.Numerics;
using System.Drawing;

namespace ARTest.Droid.Models.OpenGLs.Elements
{
    public class Line
    {
        public float LineWidth { get; set; }


        private FloatBuffer vertexBuffer;

        private const string fragmentShaderCode =
            "#version 300 es \n" +
            "precision mediump float;\n" +
            "in vec4 vColor;\n" +
            "out vec4 fragColor;\n" +
            "void main() {" +
            "  fragColor = vColor;" +
            "}";
        private string vertexShaderCode =
            // This matrix member variable provides a hook to manipulate
            // the coordinates of the objects that use this vertex shader
            "#version 300 es \n" +
            "uniform mat4 uMVPMatrix;\n" +
            "out vec4 vColor;\n" +
            "in vec4 vIColor;\n" +
            "in vec4 vPosition;\n" +
            "void main() {" +
            // the matrix must be included as a modifier of gl_Position
            // Note that the uMVPMatrix factor *must be first* in order
            // for the matrix multiplication product to be correct.
            "  gl_Position = uMVPMatrix * vPosition;" +
            "  vColor = vIColor;" +
            "}";

        // Use to access and set the view transformation
        private int vPMatrixHandle;

        float[] lineCoords = new[]
        {
            
            // Counterclockwise
             0.0f,  0.0f, 0.0f, /* Top         */ 0.99f, 0.00f, 0.00f, 1.0f,
             1.0f,  0.0f, 0.0f, /* Bottom left */ 0.00f, 0.99f, 0.00f, 1.0f,
        };

        private const int COORDS_PER_VERTEX = 3;
        private const int TOTAL_COORDS = 2;
        private int mProgram;
        private int lastGlError = 0;
        private int positionHandle;
        private int colorHandle;

        private int vertexCount = TOTAL_COORDS;// triangleCoords.Length / COORDS_PER_VERTEX;
        private int vertexStride = COORDS_PER_VERTEX * sizeof(float); // 4 bytes per vertex

        public Line(float lineWidth = 1)
        {
            LineWidth = lineWidth;
            // 4 bytes per float.
            ByteBuffer bb = ByteBuffer.AllocateDirect(lineCoords.Length * sizeof(float));

            bb.Order(ByteOrder.NativeOrder());

            vertexBuffer = bb.AsFloatBuffer();
            vertexBuffer.Put(lineCoords);
            vertexBuffer.Position(0);

            if (glHasError())
                throw new GLException(lastGlError);

            // create empty OpenGL ES Program
            mProgram = GlCreateProgram();
            if (glHasError())
                throw new GLException(lastGlError);

            int vertexShader = ShaderUtilities.LoadShader(GlVertexShader,
                                        vertexShaderCode);
            int fragmentShader = ShaderUtilities.LoadShader(GlFragmentShader,
                                            fragmentShaderCode);


            // add the vertex shader to program
            GlAttachShader(mProgram, vertexShader);
            if (glHasError())
                throw new GLException(lastGlError);

            // add the fragment shader to program
            GlAttachShader(mProgram, fragmentShader);
            if (glHasError())
                throw new GLException(lastGlError);

            // creates OpenGL ES program executables
            GlLinkProgram(mProgram);
            string msg = GlGetProgramInfoLog(mProgram);
            // Check link error as well.
            if (glHasError())
                throw new GLException(lastGlError);

            var buffer = IntBuffer.Allocate(1);
            GlGetProgramiv(mProgram, GlLinkStatus, buffer);
            int code = buffer.Get(0);
            if (code != GlTrue)
                throw new GLException(code, msg);

            positionHandle = GlGetAttribLocation(mProgram, "vPosition");
            colorHandle = GlGetAttribLocation(mProgram, "vIColor");
            vPMatrixHandle = GlGetUniformLocation(mProgram, "uMVPMatrix");

        }
        private bool glHasError()
        {
            var error = GlGetError();
            lastGlError = error;
            return error != GlNoError;
        }

        public void SetVertices(Vector3 start, Vector3 end)
        {
            int i = 0;
            lineCoords[i + 0] = start.X;
            lineCoords[i + 1] = start.Y;
            lineCoords[i + 2] = start.Z;

            i = 7;
            lineCoords[i + 0] = end.X;
            lineCoords[i + 1] = end.Y;
            lineCoords[i + 2] = end.Z;

            vertexBuffer.Clear();
            vertexBuffer.Put(lineCoords);
        }
        public void SetColor(Color color)
        {
            SetColor(color, color);
        }
        public void SetColor(Color start, Color end)
        {
            int offset = 3;
            int i = 0;
            i += offset;
            lineCoords[i + 0] = start.R / 255f;
            lineCoords[i + 1] = start.G / 255f;
            lineCoords[i + 2] = start.B / 255f;

            i = 7;
            i += offset;
            lineCoords[i + 0] = end.R / 255f;
            lineCoords[i + 1] = end.G / 255f;
            lineCoords[i + 2] = end.B / 255f;

            vertexBuffer.Clear();
            vertexBuffer.Put(lineCoords);
        }

        public void Draw(float[] mvpMatrix)
        {
            // Add program to OpenGL ES environment
            GlUseProgram(mProgram);
            if (glHasError())
            {
                string msg = GlGetProgramInfoLog(mProgram);
                printError("0 " + msg);
            }

            // Enable a handle to the triangle vertices
            // Prepare the triangle coordinate data
            vertexBuffer.Position(0);
            GlVertexAttribPointer(positionHandle, COORDS_PER_VERTEX,
                                         GlFloat, false,
                                         vertexStride + 4 * sizeof(float), vertexBuffer);
            GlEnableVertexAttribArray(positionHandle);
            if (glHasError())
                printError("3");

            vertexBuffer.Position(3);
            GlVertexAttribPointer(colorHandle, 4, GlFloat, false, vertexStride + 4 * sizeof(float), vertexBuffer);
            GlEnableVertexAttribArray(colorHandle);

            // Pass the projection and view transformation to the shader
            GlUniformMatrix4fv(vPMatrixHandle, 1, false, mvpMatrix, 0);

            GlLineWidth(LineWidth);
            // Draw the triangle
            GlDrawArrays(GlLines, 0, vertexCount);

            // Disable vertex array
            GlDisableVertexAttribArray(positionHandle);
            GlDisableVertexAttribArray(colorHandle);
        }
        private void printError(string tag = "")
        {
            System.Diagnostics.Debug.WriteLine($"GL error [{tag}]: {lastGlError} (0x{lastGlError.ToString("x")})");
        }
    }
}