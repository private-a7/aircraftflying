﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Opengl;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Nio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ARTest.Droid.Models.OpenGLs.Elements
{
    public class Image
    {
        const string TAG = "IMAGERENDERER";
        private FloatBuffer mTextureBuffer;
        private int textureId;
        public void CreateOnGlThread(Context context)
        {
            var bitmap = BitmapFactory.DecodeFile("awesome7_logo.jpg");

            //GLES20.GlEnable(GLES20.GlTexture2d);

            var textures = new int[1];
            textureId = textures[0];
            GLES20.GlGenTextures(1, textures, 0);
            GLES20.GlBindTexture(GLES20.GlTexture2d, textureId);

            GLES20.GlTexParameterf(GLES20.GlTexture2d,
                GLES20.GlTextureMagFilter,
                GLES20.GlLinear);
            GLES20.GlTexParameterf(GLES20.GlTexture2d,
                GLES20.GlTextureMinFilter,
                GLES20.GlLinear);

            float[] textureCoordinates = new[]
            {
                0.0f, 1.0f, // 0
                1.0f, 1.0f, // 1
                0.0f, 0.0f, // 2
                1.0f, 0.0f  // 3
            };
            ByteBuffer tbb = ByteBuffer.AllocateDirect(textureCoordinates.Length * 4);
            tbb.Order(ByteOrder.NativeOrder());
            mTextureBuffer = tbb.AsFloatBuffer();
            mTextureBuffer.Put(textureCoordinates);
            mTextureBuffer.Position(0);

            GLUtils.TexImage2D(GLES20.GlTexture2d, 0, bitmap, 0);

            bitmap.Dispose();
        }

        public void Draw(GLES20 gl)
        {

        }
    }
    /// <summary>
    /// Downloaded and updated code for OpenGL ES 3.0
    /// </summary>
    /// <seealso cref="https://github.com/ibraimgm/opengles2-2d-demos/blob/master/src/ibraim/opengles2/TextureActivity.java"/>
    public class TextureRenderer
    {
        private int vertexHandle;
        private int fragmentHandle;
        private int programHandle = -1;
        private int[] textures = new int[1];

        // Vertex shader source.
        // Now things start to get interesting. Take note of a new attribute,
        // aTexPos, that will store the texture coordinate (the "places" of the texture that
        // we will use. We also have vTexPos, to pass the attribute value to the
        // fragment shader.
        private string vertexSrc =
            // This matrix member variable provides a hook to manipulate
            // the coordinates of the objects that use this vertex shader
            /*"#version 300 es \n" +
            "uniform mat4 uMVPMatrix;\n" +
            "out vec4 vColor;\n" +
            "in vec4 vIColor;\n" +
            "in vec4 vPosition;\n" +
            "void main() {" +
            // the matrix must be included as a modifier of gl_Position
            // Note that the uMVPMatrix factor *must be first* in order
            // for the matrix multiplication product to be correct.
            "  gl_Position = uMVPMatrix * vPosition;" +
            "  vColor = vIColor;" +
            "}";*/
        "#version 300 es \n" +
         "uniform mat4 uScreen;\n" +
         "in vec2 aPosition;\n" +
         "in vec2 aTexPos;\n" +
         "out vec2 vTexPos;\n" +
         "void main() {\n" +
         "  vTexPos = aTexPos;" +
         "  gl_Position = uScreen * vec4(aPosition.xy, 0.0, 1.0);" +
         "}";

        // These two methods help to Load/Unload the shaders used by OpenGL Es 2.0
        // Remember that now OpenGL DOES NOT CONTAIN most of the 'old' OpenGL functions;
        // Now you need to create your own vertex and fragment shaders. Yay!
        public void setup()
        {
            // make sure there's nothing already created
            tearDown();

            

            // Our fragment shader.
            // Here we have a uniform (uTexture) that will hold the texture
            // for drawing. The 'color' of the vertex is calculated using the
            // texture coordinate (vTexPos) and the texture itself.
            string fragmentSrc =
                "#version 300 es \n" +
              "precision mediump float;\n" +
              "uniform sampler2D uTexture;\n" +
              "in vec2 vTexPos;\n" +
              "out vec4 vColor;\n" +
              "void main(){" +
              "  vColor = texture(uTexture, vTexPos);" +
              "}";

            // Lets load and compile our shaders, link the program
            // and tell OpenGL ES to use it for future drawing.
            vertexHandle = loadShader(GLES20.GlVertexShader, vertexSrc);
            fragmentHandle = loadShader(GLES20.GlFragmentShader, fragmentSrc);
            programHandle = createProgram(vertexHandle, fragmentHandle);

            GLES20.GlUseProgram(programHandle);
        }

        public void tearDown()
        {
            if (programHandle != -1)
            {
                GLES20.GlDeleteProgram(programHandle);
                GLES20.GlDeleteShader(vertexHandle);
                GLES20.GlDeleteShader(fragmentHandle);
                GLES20.GlDeleteTextures(textures.Length, textures, 0); // free the texture!
            }
        }

        // auxiliary shader functions. Doesn't matter WHAT you're trying to do, they're
        // always the same thing.
        private int loadShader(int shaderType, String shaderSource)
        {
            int handle = GLES20.GlCreateShader(shaderType);

            //if (handle != GLES20.GlNoError)
                //throw new ApplicationException("Error creating shader!");

            // set and compile the shader
            GLES20.GlShaderSource(handle, shaderSource);
            GLES20.GlCompileShader(handle);

            // check if the compilation was OK
            int[] compileStatus = new int[1];
            GLES20.GlGetShaderiv(handle, GLES20.GlCompileStatus, compileStatus, 0);

            if (compileStatus[0] != GLES20.GlTrue)
            {
                string error = GLES20.GlGetShaderInfoLog(handle);
                GLES20.GlDeleteShader(handle);
                throw new ApplicationException("Error compiling shader: " + error);
            }
            else
                return handle;
        }

        private int createProgram(int vertexShader, int fragmentShader)
        {
            int handle = GLES20.GlCreateProgram();

            if (GLES20.GlGetError() != GLES20.GlNoError)
            //if (handle != GLES20.GlNoError)
                throw new ApplicationException("Error creating program!");

            // attach the shaders and link the program
            GLES20.GlAttachShader(handle, vertexShader);
            GLES20.GlAttachShader(handle, fragmentShader);
            GLES20.GlLinkProgram(handle);

            // check if the link was successful
            int[] linkStatus = new int[1];
            GLES20.GlGetProgramiv(handle, GLES20.GlLinkStatus, linkStatus, 0);

            if (linkStatus[0] != GLES20.GlTrue)
            {
                String error = GLES20.GlGetProgramInfoLog(handle);
                GLES20.GlDeleteProgram(handle);
                throw new ApplicationException("Error in program linking: " + error);
            }
            else
                return handle;
        }

        public void onSurfaceCreated(Context context)
        {
            // first, try to generate a texture handle
            GLES20.GlGenTextures(1, textures, 0);

            //if (textures[0] != GLES20.GlNoError)
               // throw new ApplicationException("Error loading texture");

            // bind the texture and set parameters
            GLES20.GlBindTexture(GLES20.GlTexture2d, textures[0]);
            GLES20.GlTexParameteri(GLES20.GlTexture2d, GLES20.GlTextureMinFilter, GLES20.GlNearest);
            GLES20.GlTexParameteri(GLES20.GlTexture2d, GLES20.GlTextureMagFilter, GLES20.GlNearest);

            // Load a bitmap from resources folder and pass it to OpenGL
            // in the end, we recycle it to free unneeded resources
            //Bitmap b = BitmapFactory.DecodeResource(getResources(), R.drawable.skull);
            var imageName = "awesome7_logo.jpg";
            //var imagePath = new File( getFilesDir(), imageName).getAbsolutePath();
            Bitmap b = BitmapFactory.DecodeResource(context.Resources, Resource.Drawable.awesome7_logo);
            GLUtils.TexImage2D(GLES20.GlTexture2d, 0, b, 0);
            b.Dispose();
        }

        public void onSurfaceChanged(int width, int height)
        {
            // lets initialize everything
            setup();

            // discover the 'position' of the uScreen and uTexture
            int uScreenPos = GLES20.GlGetUniformLocation(programHandle, "uScreen");
            int uTexture = GLES20.GlGetUniformLocation(programHandle, "uTexture");

            // The uScreen matrix
            // This is explained in detail in the Triangle2d sample.
            float[] uScreen =
            {
                2f/width/3,   0f,         0f, 0f,
                0f,        -2f/height/3,  0f, 0f,
                0f,         0f,         0f, 0f,
               -1f,         1f,         0f, 1f
            };

            // Now, let's set the value.
            FloatBuffer b = ByteBuffer.AllocateDirect(uScreen.Length * 4).Order(ByteOrder.NativeOrder()).AsFloatBuffer();
            b.Put(uScreen).Position(0);
            GLES20.GlUniformMatrix4fv(uScreenPos, b.Limit() / uScreen.Length, false, b);

            // Activate the first texture (GL_TEXTURE0) and bind it to our handle
            GLES20.GlActiveTexture(GLES20.GlTexture0);
            GLES20.GlBindTexture(GLES20.GlTexture2d, textures[0]);
            GLES20.GlUniform1i(uTexture, 0);

            // set the viewport and a fixed, white background
            //GLES20.GlViewport(0, 0, width/3, height/3);
            //GLES20.GlClearColor(1f, 1f, 1f, 1f);

            // since we're using a PNG file with transparency, enable alpha blending.
            GLES20.GlBlendFunc(GLES20.GlSrcAlpha, GLES20.GlOneMinusSrcAlpha);
            GLES20.GlEnable(GLES20.GlBlend);
        }

        public void onDrawFrame()
        {
            // get the position of our attributes
            int aPosition = GLES20.GlGetAttribLocation(programHandle, "aPosition");
            int aTexPos = GLES20.GlGetAttribLocation(programHandle, "aTexPos");

            // Ok, now is the FUN part.
            // First of all, our image is a rectangle right? but in OpenGL, we can only draw
            // triangles! To remedy that we will use 4 vertices (V1 to V4) and draw using
            // the TRIANGLE_STRIP option. If you look closely to our positions, you will note
            // that we're drawing a 'N' (or 'Z') shaped line... and TRIANGLE_STRIP 'closes' the
            // remaining GAP between the vertices, so we have a rectangle (or square)! Yay!
            //
            // Apart from V1 to V4, we also specify the position IN THE TEXTURE. Each vertex
            // of our rectangle must relate to a position in the texture. The texture coordinates
            // are ALWAYS 0,0 on bottom-left and 1,1 on top-right. Take a look at the values
            // used and you will understand it easily. If not, mess a little bit with the values
            // and take a look at the result.
            float[] data =
            {
                10f,50f,  //V1
                0f, 0f,     //Texture coordinate for V1

                20f, 80f,  //V2
                0f,  1f,

                300f, 50f, //V3
                1f, 0f,

                100f, 100f,  //V4
                1f, 1f
            };

            // constants. You know the drill by now.
            int FLOAT_SIZE = 4;
            int POSITION_SIZE = 2;
            int TEXTURE_SIZE = 2;
            int TOTAL_SIZE = POSITION_SIZE + TEXTURE_SIZE;
            int POSITION_OFFSET = 0;
            int TEXTURE_OFFSET = 2;

            // Again, a FloatBuffer will be used to pass the values
            FloatBuffer b = ByteBuffer.AllocateDirect(data.Length * FLOAT_SIZE).Order(ByteOrder.NativeOrder()).AsFloatBuffer();
            b.Put(data);

            // Position of our image
            b.Position(POSITION_OFFSET);
            GLES20.GlVertexAttribPointer(aPosition, POSITION_SIZE, GLES20.GlFloat, false, TOTAL_SIZE * FLOAT_SIZE, b);
            GLES20.GlEnableVertexAttribArray(aPosition);

            // Positions of the texture
            b.Position(TEXTURE_OFFSET);
            GLES20.GlVertexAttribPointer(aTexPos, TEXTURE_SIZE, GLES20.GlFloat, false, TOTAL_SIZE * FLOAT_SIZE, b);
            GLES20.GlEnableVertexAttribArray(aTexPos);

            // Clear the screen and draw the rectangle
            //GLES20.GlClear(GLES20.GlColorBufferBit);
            GLES20.GlDrawArrays(GLES20.GlTriangleStrip, 0, 4);
        }
    }
}