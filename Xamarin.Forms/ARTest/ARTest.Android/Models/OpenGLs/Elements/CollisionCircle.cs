﻿using Android.App;
using Android.Arch.Lifecycle;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ARTest.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace ARTest.Droid.Models.OpenGLs.Elements
{
    class CollisionCircle
    {
        public float Sensitivity = 1.0f;
        private Aircraft aircraft;
        private float modelLength;
        private float modelWidth;
        private Circle circle;
        private Color collidedColor;
        private Color defaultColor;
        public CollisionCircle(Aircraft air, Circle c, Color circleCollidedColor)
        {
            aircraft = air;
            circle = c;
            this.modelLength = air.ModelLength * air.ModelScale;
            this.modelWidth = air.ModelWidth * air.ModelScale;
            collidedColor = circleCollidedColor;
            defaultColor = c.Color;
        }

        public void CheckCollision()
        {
            bool collided = false;
            var a = aircraft;
            var d = a.Position.DistanceTo(circle.Center);
            if (d < Sensitivity)
                collided = true;
            d = a.Position.Add(a.Direction * (float)(modelLength / 2)).DistanceTo(circle.Center);
            if (d < Sensitivity)
                collided = true;
            d = a.Position.Add(a.Direction * (float)(-modelLength / 2)).DistanceTo(circle.Center);
            if (d < Sensitivity)
                collided = true;

            if (collided)
                circle.SetColor(collidedColor);
            else
                circle.SetColor(defaultColor);
        }
        public void DrawCircle(float[] vpMatrix)
        {
            circle.Draw(vpMatrix);
        }
    }
}