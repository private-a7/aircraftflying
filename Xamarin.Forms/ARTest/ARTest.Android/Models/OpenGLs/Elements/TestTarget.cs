﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;
using System.Drawing;
using Android.Opengl;
using Google.AR.Core;
using ARTest.Geometry;
using Xamarin.Essentials;
using ARTest.Services.Sensors;

namespace ARTest.Droid.Models.OpenGLs.Elements
{
    /// <summary>
    /// Target drawn at North direction, at specific distance from us.
    /// </summary>
    public class TestTarget
    {
        /// <summary>
        /// Distance to target.
        /// </summary>
        public float Distance { get; private set; } = 100;
        /// <summary>
        /// Height relative to camera.
        /// </summary>
        public float Height { get; private set; } = 20;
        /// <summary>
        /// Compass delta which is tolerated (considered as unchanged).
        /// </summary>
        public float AngleDelta { get; private set; } = 5;
        
        /// <summary>
        /// Width of target side.
        /// </summary>
        public float Width { get; private set; } = 10;

        private AVector3 xAxis = new AVector3(1, 0, 0);
        private AVector3 yAxis = new AVector3(0, 1, 0);
        private AVector3 zAxis = new AVector3(0, 0, 1);

        private Triangle leftTriangle = new Triangle();
        private Triangle rightTriangle = new Triangle();
        private Triangle topTriangle = new Triangle();


        private double lastCompassHeading = 0;
        private bool isTracking = false;
        private AVector3 lastCameraDistance = new AVector3();
        
        public TestTarget()
        {

            leftTriangle.SetColor(Color.Red);
            rightTriangle.SetColor(Color.Green);
            topTriangle.SetColor(Color.Blue);
            updateVertices();
            CompassMonitor.StartTracking();
        }
        public void SetLength(float length)
        {
            xAxis = xAxis.GetUnit() * length;
            yAxis = yAxis.GetUnit() * length;
            zAxis = zAxis.GetUnit() * length;
            updateVertices();
        }
        /// <summary>
        /// Invoke this when ARCore stops tracking.
        /// </summary>
        public void Refresh(TrackingState currentState, Pose pose)
        {

            // Update values only when tracking resumes.
            if (!isTracking && currentState == TrackingState.Tracking)
            {
                isTracking = true;
            }
            else
            {
                isTracking = currentState == TrackingState.Tracking;
                return;
            }
            AVector3 cameraTranslation = pose.GetTranslation().ToVector();

            // If tracking, and new state is not tracked - disable.
            AVector3 xAxis = pose.GetXAxis().ToVector().GetUnit();
            AVector3 yAxis = pose.GetYAxis().ToVector().GetUnit();
            AVector3 zAxis = pose.GetZAxis().ToVector().GetUnit();


            // Create axis in XZ plane, rotate 90deg and this direction of camera.
            // If y axis of ARCore, and Y axis of camera match, then 
            // negative Z axis is direction of camera.
            // Use compass to calculate direction of target.
            // Calculate vector from camera to target, and translate target to final position.
            // Target needs to be drawn in x axis of the camera?

            // Axis in XZ plane (y direction is same for both systems.
            AVector3 xzAxis = yAxis.CrossProduct(AVector3.YAxis);
            // Direction is front from camera, in XZ plane of ARCore (parallel to ground).
            AVector3 direction = xzAxis;
            // If length is 0, both y axes match.
            if (direction.Length < 1e-3)
                direction = zAxis.Negate();
            else
            {
                var matrix = Matrix4x4.CreateRotationY((float)Math.PI / 2);
                direction = Vector3.Transform(direction, matrix).ToAVector3();
            }

            double angleDeg = CompassMonitor.AngleDeg;
            //if (Math.Abs(angleDeg - lastCompassHeading) < AngleDelta)
                //angleDeg = lastCompassHeading;
            lastCompassHeading = angleDeg;
            double angleRad = angleDeg * Math.PI / 180;
            if (zAxis.Y < 0)
            {
                // Phone is tilted back (looking up).
                //angleRad += Math.PI;
            }

            Matrix4x4 rotation = Matrix4x4.CreateRotationY((float)angleRad);
            AVector3 north = Vector3.Transform(direction, rotation).ToAVector3().GetUnit();

            //TODO Add offset from north.
            //TODO Calculate angle offset of GPS coordinate, from north in our position.

            AVector3 sourceToTarget = north * Distance + new AVector3(0, Height, 0);
            AVector3 targetVector = cameraTranslation + sourceToTarget;

            rotation = Matrix4x4.CreateRotationY((float)Math.PI / 2);
            var xAxisTarget = Vector3.Transform(sourceToTarget, rotation).ToAVector3();
            var yAxisTarget = new AVector3(0, 1, 0);

            updateVertices(targetVector, xAxisTarget, yAxisTarget);

        }
        public void Draw(float[] mvpMatrix, Pose pose)
        {
            leftTriangle.Draw(mvpMatrix);
            rightTriangle.Draw(mvpMatrix);
            topTriangle.Draw(mvpMatrix);

        }
        private void updateVertices(AVector3? translate = null, AVector3? xAxis = null, AVector3? yAxis = null)
        {
            var x = xAxis ?? AVector3.XAxis;
            var y = yAxis ?? AVector3.YAxis;
            var t = translate ?? new AVector3(0, 0, 0);
            x = x.GetUnit();
            y = y.GetUnit();

            float w = Width;
            float l = w / 2;
            AVector3 left1 = x * -l + y * -l;// new Vector3(-l, -l, 0);
            AVector3 left2 = y * -l;//  new Vector3(0, -l, 0);
            AVector3 left3 = x * -l + y * l;// new Vector3(-l, l, 0);

            AVector3 right1 = x * l + y * -l;// new Vector3(l, -l, 0);
            AVector3 right2 = y * -l;// new Vector3(0, -l, 0);
            AVector3 right3 = x * l + y * l;// new Vector3(l, l, 0);

            AVector3 top1 = x * -l + y * l;// new Vector3(-l, l, 0);
            AVector3 top2 = x * l + y * l;// new Vector3(l, l, 0);
            AVector3 top3 = y * (l / 3f);// new Vector3(0, l/3f, 0);

            left1 = left1 + t;
            left2 = left2 + t;
            left3 = left3 + t;

            right1 = right1 + t;
            right2 = right2 + t;
            right3 = right3 + t;

            top1 = top1 + t;
            top2 = top2 + t;
            top3 = top3 + t;

            leftTriangle.SetVertices(left1, left2, left3);
            rightTriangle.SetVertices(right1, right2, right3); ;
            topTriangle.SetVertices(top1, top2, top3); ;
        }
    }
}