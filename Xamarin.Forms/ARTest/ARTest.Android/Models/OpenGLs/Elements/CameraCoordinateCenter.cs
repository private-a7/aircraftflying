﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;
using System.Drawing;
using Android.Opengl;

namespace ARTest.Droid.Models.OpenGLs.Elements
{
    /// <summary>
    /// Coordinate center displayed in fron of camera (screen coordinate system).
    /// </summary>
    public class CameraCoordinateCenter
    {
        private Line xLine = new Line();
        private Line yLine = new Line();
        private Line zLine = new Line();

        private Vector3 xAxis = new Vector3(1, 0, 0);
        private Vector3 yAxis = new Vector3(0, 1, 0);
        private Vector3 zAxis = new Vector3(0, 0, 1);
        private float[] identityMatrix = new float[16];
        public CameraCoordinateCenter()
        {
            Matrix.SetIdentityM(identityMatrix, 0);

            xLine.SetColor(Color.Red, Color.LightBlue);
            yLine.SetColor(Color.Green, Color.LightBlue);
            zLine.SetColor(Color.Blue, Color.LightBlue);

            Vector3 v = new Vector3();
            xLine.SetVertices(v, xAxis);
            yLine.SetVertices(v, yAxis);
            zLine.SetVertices(v, zAxis);
        }
        public void SetLength(float length)
        {
            xAxis = Vector3.Normalize(xAxis) * length;
            yAxis = Vector3.Normalize(yAxis) * length;
            zAxis = Vector3.Normalize(zAxis) * length;
        }
        public void Draw()
        {
            xLine.Draw(identityMatrix);
            yLine.Draw(identityMatrix);
            zLine.Draw(identityMatrix);

        }
    }
}