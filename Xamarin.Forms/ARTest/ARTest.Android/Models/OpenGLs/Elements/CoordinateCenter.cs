﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;
using System.Drawing;

namespace ARTest.Droid.Models.OpenGLs.Elements
{
    public class CoordinateCenter
    {
        private Line xLine = new Line();
        private Line yLine = new Line();
        private Line zLine = new Line();

        private Vector3 center = new Vector3();
        private Vector3 xAxis = new Vector3(1, 0, 0);
        private Vector3 yAxis = new Vector3(0, 1, 0);
        private Vector3 zAxis = new Vector3(0, 0, 1);

        private Triangle xyTriangle = new Triangle();
        private Triangle yzTriangle = new Triangle();
        private Triangle zxTriangle = new Triangle();
        public CoordinateCenter()
        {

            xLine.SetColor(Color.Red, Color.LightBlue);
            yLine.SetColor(Color.Green, Color.LightBlue);
            zLine.SetColor(Color.Blue, Color.LightBlue);

            xyTriangle.SetColor(Color.Red);
            yzTriangle.SetColor(Color.Green);
            zxTriangle.SetColor(Color.Blue);
            updateVertices();
        }
        /// <summary>
        /// Translates coordinate center (initially from (0,0,0)).
        /// </summary>
        /// <param name="center"></param>
        public void SetNewCenter(Vector3 center)
        {
            this.center = center;
            updateVertices();
        }
        /// <summary>
        /// Sets new axes for a system.
        /// <para>(Should pass transformation Matrix in here)</para>
        /// </summary>
        public void SetNewSystem(Vector3 axisX, Vector3 axisY, Vector3 axisZ)
        {
            xAxis = axisX;
            yAxis = axisY;
            zAxis = axisZ;
            updateVertices();
        }
        public void SetLength(float length)
        {
            xAxis = Vector3.Normalize(xAxis) * length;
            yAxis = Vector3.Normalize(yAxis) * length;
            zAxis = Vector3.Normalize(zAxis) * length;
            updateVertices();
        }
        public void Draw(float[] mvpMatrix)
        {
            xLine.Draw(mvpMatrix);
            yLine.Draw(mvpMatrix);
            zLine.Draw(mvpMatrix);

            xyTriangle.Draw(mvpMatrix);
            yzTriangle.Draw(mvpMatrix);
            zxTriangle.Draw(mvpMatrix);
        }
        private void updateVertices()
        {
            xLine.SetVertices(center, xAxis);
            yLine.SetVertices(center, yAxis);
            zLine.SetVertices(center, zAxis);

            float l = 0.05f;
            Vector3 c = center;
            var x = Vector3.Normalize(xAxis);
            var y = Vector3.Normalize(yAxis);
            var z = Vector3.Normalize(zAxis);
            xyTriangle.SetVertices(c + (x * l), c + (y * l), c);
            yzTriangle.SetVertices(c, c + (y * l), c + (z * l));
            zxTriangle.SetVertices(c + (x * l), c, c + (z * l));
        }
    }
}