﻿using Android.App;
using Android.Content;
using Android.Opengl;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Nio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Android.Opengl.GLES30;
using static Android.Opengl.GLES20;
using System.Numerics;
using System.Drawing;
using ARTest.Geometry;

namespace ARTest.Droid.Models.OpenGLs.Elements
{
    public class Circle
    {
        public float LineWidth { get; set; }
        public float Diameter { get; private set; }
        public float Radius { get; private set; }
        public AVector3 Center { get; private set; } = new AVector3();
        public AVector3 Normal { get; private set; } = new AVector3(0,0,-1);
        public Color Color { get; private set; } = Color.White;

        /// <summary>
        /// Transformation Matrix (
        /// </summary>
        private float[] transform = new float[16];
        private FloatBuffer vertexBuffer;

        private const string fragmentShaderCode =
            "#version 300 es \n" +
            "precision mediump float;\n" +
            "in vec4 vColor;\n" +
            "out vec4 fragColor;\n" +
            "void main() {" +
            "  fragColor = vColor;" +
            "}";
        private string vertexShaderCode =
            // This matrix member variable provides a hook to manipulate
            // the coordinates of the objects that use this vertex shader
            "#version 300 es \n" +
            "uniform mat4 uMVPMatrix;\n" +
            "out vec4 vColor;\n" +
            "in vec4 vIColor;\n" +
            "in vec4 vPosition;\n" +
            "void main() {" +
            // the matrix must be included as a modifier of gl_Position
            // Note that the uMVPMatrix factor *must be first* in order
            // for the matrix multiplication product to be correct.
            "  gl_Position = uMVPMatrix * vPosition;" +
            "  vColor = vIColor;" +
            "}";

        // Use to access and set the view transformation
        private int vPMatrixHandle;

        /// <summary>
        /// Single array to pass to OpenGL. It contains both 
        /// coordinates and color (for each coordinate).
        /// </summary>
        /// <remarks>
        /// Organization of array (each stride has 7 float elements):
        /// One vertex:
        /// |x(float);y(float);z(float);rgb0(float*4)| 
        /// Each 2 neighbour vertices make 1 line (0-1, 1-2, ...).
        /// </remarks>
        float[] lineCoords = new float[0];

        private const int COORDS_PER_VERTEX = 3;
        private int mProgram;
        private int lastGlError = 0;
        private int positionHandle;
        private int colorHandle;

        private byte vertexCount = 19; // n-1 lines. (20 and 18 miss 1 line).
        private int vertexStride = COORDS_PER_VERTEX * sizeof(float); // 4 bytes per vertex
        private int colorSize = 4;

        public Circle(float lineWidth = 1)
        {
            transform.SetIdentity();
            
            LineWidth = lineWidth;
            // 4 bytes per float.
            ByteBuffer bb = ByteBuffer.AllocateDirect(lineCoords.Length * sizeof(float));

            bb.Order(ByteOrder.NativeOrder());

            vertexBuffer = bb.AsFloatBuffer();
            vertexBuffer.Put(lineCoords);
            vertexBuffer.Position(0);

            if (glHasError())
                throw new GLException(lastGlError);

            // create empty OpenGL ES Program
            mProgram = GlCreateProgram();
            if (glHasError())
                throw new GLException(lastGlError);

            int vertexShader = ShaderUtilities.LoadShader(GlVertexShader,
                                        vertexShaderCode);
            int fragmentShader = ShaderUtilities.LoadShader(GlFragmentShader,
                                            fragmentShaderCode);


            // add the vertex shader to program
            GlAttachShader(mProgram, vertexShader);
            if (glHasError())
                throw new GLException(lastGlError);

            // add the fragment shader to program
            GlAttachShader(mProgram, fragmentShader);
            if (glHasError())
                throw new GLException(lastGlError);

            // creates OpenGL ES program executables
            GlLinkProgram(mProgram);
            string msg = GlGetProgramInfoLog(mProgram);
            // Check link error as well.
            if (glHasError())
                throw new GLException(lastGlError);

            var buffer = IntBuffer.Allocate(1);
            GlGetProgramiv(mProgram, GlLinkStatus, buffer);
            int code = buffer.Get(0);
            if (code != GlTrue)
                throw new GLException(code, msg);

            positionHandle = GlGetAttribLocation(mProgram, "vPosition");
            colorHandle = GlGetAttribLocation(mProgram, "vIColor");
            vPMatrixHandle = GlGetUniformLocation(mProgram, "uMVPMatrix");


            //SetOrientation(new AVector3(0,0,0), new AVector3(0, 0, -1));
        }
        private bool glHasError()
        {
            var error = GlGetError();
            lastGlError = error;
            return error != GlNoError;
        }

        public void SetRadius(float r)
        {
            Radius = r;
            Diameter = r * 2;
        }
        public void SetDiameter(float d) => SetRadius(d / 2);
        public void SetOrientation(AVector3 center, AVector3 normal)
        {
            setDirection(transform, normal);
            Center = center;

            // Create circle around Z axis in WCS.
            // Transform point from WCS to OCS.

            int offset = COORDS_PER_VERTEX;
            byte n = vertexCount;
            lineCoords = new float[(n) * (COORDS_PER_VERTEX + colorSize)];
            float angleStep = 360 / (n - 1);
            // Rotation matrix in WCS.
            var matrixRotation = Matrix4x4.CreateRotationZ((float)(angleStep * Math.PI / 180));
            // Transformation matrix (from WCS to OCS).
            var matrixTr = transform.ToMatrix();
            // Tranform from OCS to WCS.
            var right = new Vector3(Radius, 0, 0);
            for (int i = 0; i < lineCoords.Length; i += colorSize)
            {
                //if (i > 0)
                right = Vector3.Transform(right, matrixRotation);
                var t = Vector3.Transform(right, matrixTr);// Center + right;
                t = t + Center;
                lineCoords.SetVector(i, t.ToAVector3());
                i += offset;
            }
            //lineCoords.SetVector(n * 7, lineCoords.GetVector(0));
            SetColor(Color, true);
            refreshBuffer(lineCoords);
        }
        public void SetColor(Color c, bool forceUpdate = false)
        {
            if (!forceUpdate && c == Color)
                return;
            Color = c;
            int offset = COORDS_PER_VERTEX;
            for(int i = 0; i < lineCoords.Length; i+=colorSize)
            {
                i += offset;
                lineCoords[i + 0] = c.R / 255f;
                lineCoords[i + 1] = c.G / 255f;
                lineCoords[i + 2] = c.B / 255f;
            }
            refreshBuffer(lineCoords);
        }
        private void refreshBuffer(float[] array)
        {
            ByteBuffer bb = ByteBuffer.AllocateDirect(array.Length * sizeof(float));

            bb.Order(ByteOrder.NativeOrder());

            vertexBuffer = bb.AsFloatBuffer();
            vertexBuffer.Put(array);
            vertexBuffer.Position(0);
        }

        private void setDirection(float[] matrix, AVector3 v)
        {
            var vector = v;
            var right = vector * AVector3.YAxis;
            var up = right * vector;
            matrix.SetIdentity();
            matrix.SetVector(0, right);
            matrix.SetVector(4, up);
            matrix.SetVector(8, vector);
            matrix.SetTranslation(Center);
            Normal = vector.GetUnit();
        }

        public void Draw(float[] mvpMatrix)
        {
            // Add program to OpenGL ES environment
            GlUseProgram(mProgram);
            if (glHasError())
            {
                string msg = GlGetProgramInfoLog(mProgram);
                printError("0 " + msg);
            }

            // Enable a handle to the triangle vertices
            // Prepare the triangle coordinate data
            vertexBuffer.Position(0);
            GlVertexAttribPointer(positionHandle, COORDS_PER_VERTEX,
                                         GlFloat, false,
                                         vertexStride + 4 * sizeof(float), vertexBuffer);
            GlEnableVertexAttribArray(positionHandle);
            if (glHasError())
                printError("3");

            vertexBuffer.Position(3);
            GlVertexAttribPointer(colorHandle, 4, GlFloat, false, vertexStride + 4 * sizeof(float), vertexBuffer);
            GlEnableVertexAttribArray(colorHandle);

            // Pass the projection and view transformation to the shader
            GlUniformMatrix4fv(vPMatrixHandle, 1, false, mvpMatrix, 0);

            GlLineWidth(LineWidth);
            // Draw the lines
            GlDrawArrays(GlLineStrip, 0, vertexCount);
            if (glHasError())
                printError("4");
            GlLineWidth(1);

            // Disable vertex array
            GlDisableVertexAttribArray(positionHandle);
            GlDisableVertexAttribArray(colorHandle);
        }
        private void printError(string tag = "")
        {
            System.Diagnostics.Debug.WriteLine($"GL error [{tag}]: {lastGlError} (0x{lastGlError.ToString("x")})");
        }
    }
}