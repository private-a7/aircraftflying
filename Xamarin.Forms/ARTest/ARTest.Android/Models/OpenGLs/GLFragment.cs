﻿using Android.Content;
using Android.Opengl;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARTest.Droid.Models.OpenGLs
{
    public class GLFragment : Fragment
    {
        public MyGLSurfaceView SurfaceView { get; private set; }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            if (container == null)
                return null;
            
            var glView = new MyGLSurfaceView(Activity);
            SurfaceView = glView;
            glView.Left = 70;
            glView.Top = 70;
            glView.SetMinimumHeight(200);
            glView.SetMinimumWidth(200);
            // MUST NOT BE SET!!!
            //glView.SetBackgroundColor(Android.Graphics.Color.Green);
            
            return glView;
            //return base.OnCreateView(inflater, container, savedInstanceState);
        }
    }
}