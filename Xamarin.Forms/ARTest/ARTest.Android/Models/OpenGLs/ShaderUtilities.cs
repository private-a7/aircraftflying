﻿using Android.App;
using Android.Content;
using Android.Opengl;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using static Android.Opengl.GLES20;
using static Android.Opengl.GLES30;

namespace ARTest.Droid.Models.OpenGLs
{
	public static class ShaderUtilities
	{
		/// <summary>
		/// Loads shader from resource.
		/// </summary>
		public static int LoadGLShader(string tag, Context context, int type, int resId)
		{
			var code = ReadRawTextFile(context, resId);
			var shader = GLES20.GlCreateShader(type);

			GLES20.GlShaderSource(shader, code);
			GLES20.GlCompileShader(shader);

			var compileStatus = new int[1];
			GLES20.GlGetShaderiv(shader, GLES20.GlCompileStatus, compileStatus, 0);

			if (compileStatus[0] == 0)
			{
				GLES20.GlDeleteShader(shader);
				shader = 0;
			}

			if (shader == 0)
				throw new Exception("Error creating shader");

			return shader;
		}
		/// <summary>
		/// Loads shader from code (stored as string).
		/// </summary>
		/// <param name="type"></param>
		/// <param name="shaderCode"></param>
		/// <returns></returns>
		public static int LoadShader(int type, string shaderCode)
		{

			// create a vertex shader type (GLES20.GL_VERTEX_SHADER)
			// or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
			int shader = GlCreateShader(type);

			// add the source code to the shader and compile it
			GlShaderSource(shader, shaderCode);
			GlCompileShader(shader);
			// Should get result of compilation.
			var buffer = new int[1];
			GlGetShaderiv(shader, GlCompileStatus, buffer, 0);
			int code = buffer[0];
			if (code != GlTrue)
			{
				string msg = GlGetShaderInfoLog(shader);
				GlDeleteShader(shader);
				Log.Error("GLSL", msg);
				throw new GLSLException($"GLSL Compile error: \n{msg}");
			}

			return shader;
		}

		public static void CheckGLError(string tag, string label)
		{
			int error;
			while ((error = GLES20.GlGetError()) != GLES20.GlNoError)
			{
				Log.Error(tag, label + ": glError " + error);
				throw new Exception(label + ": glError " + error);
			}
		}


		static string ReadRawTextFile(Context context, int resId)
		{
			string result = null;

			using (var rs = context.Resources.OpenRawResource(resId))
			using (var sr = new StreamReader(rs))
			{
				result = sr.ReadToEnd();
			}

			return result;
		}
	}
}