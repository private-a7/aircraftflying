﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARTest.Droid.Models.OpenGLs
{
    public class GLSLException : System.Exception
    {
        public GLSLException(string msg) : base(msg)
        {
        }
    }
}