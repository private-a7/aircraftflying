﻿using Android.App;
using Android.Content;
using Android.Opengl;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms.Platform.Android;

namespace ARTest.Droid.Models.OpenGLs
{
    /// <summary>
    /// Suggested by documentation to extend main class.
    /// </summary>
    public class MyGLSurfaceView : GLSurfaceView
    {
        
        public View.IOnTouchListener TouchListener { get; }

        private GLSurfaceViewRenderer renderer;
        public MyGLSurfaceView(Context context) : base(context)
        {
            SetEGLContextClientVersion(2);
            var activity = (MainActivity)context.GetActivity();
            renderer = new GLSurfaceViewRenderer(activity, activity.ARManager.Session);
            SetRenderer(renderer);
            RenderMode = Rendermode.Continuously;

            var touch = new TouchListener();

            TouchListener = touch;
            touch.Touched += (v, e) => renderer.Gesture.OnTouchEvent(e);
            SetOnTouchListener(TouchListener);

            SetMinimumWidth(150);
            SetMinimumHeight(250);

            // MUST NOT BE SET!!!
            //SetBackgroundColor(Android.Graphics.Color.Red);

            
        }

    }
}