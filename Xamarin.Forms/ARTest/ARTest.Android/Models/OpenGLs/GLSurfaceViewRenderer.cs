﻿using Android.App;
using Android.Content;
using Android.Opengl;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using ARTest.Droid.Models.OpenGLs.Elements;
using ARTest.Services;
using Google.AR.Core;
using Java.Interop;
using Java.Nio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using kes = Javax.Microedition.Khronos.Opengles;
using static Android.Opengl.GLES20;
using static Android.Opengl.GLES30;
using System.Numerics;
using ARTest.Droid.Ar;
using Xamarin.Forms.Xaml;
using System.Diagnostics;
using ARTest.Models;
using ARTest.Services.Sensors;
using System.Drawing;
using ARTest.Geometry;

namespace ARTest.Droid.Models.OpenGLs
{
    public class GLSurfaceViewRenderer : Java.Lang.Object, GLSurfaceView.IRenderer
    {
        public event Action FrameDrawn;

        public Session ARSession { get; }
        public GestureDetector Gesture { get; }

        private BackgroundRenderer mBackgroundRenderer = new BackgroundRenderer();
        private PointCloudRenderer cloudRenderer = new PointCloudRenderer();
        private DisplayRotationHelper rotationHelper;
        private ObjectRenderer mVirtualObject = new ObjectRenderer();

        private ARDataService dataService;
        private List<Triangle> fixedTriangles = new List<Triangle>();
        private CoordinateCenter worldCenter;
        private CameraCoordinateCenter cameraCenter;
        private List<CollisionCircle> circles = new List<CollisionCircle>();

        // vPMatrix is an abbreviation for "Model View Projection Matrix"
        private float[] vPMatrix = new float[16];
        private float[] projectionMatrix = new float[16];
        private float[] viewMatrix = new float[16];
        private Context context;

        private FramerateCounter framerate = new FramerateCounter();
        private AutoPilot pilot = new AutoPilot();


        private TestTarget testTarget;
        public GLSurfaceViewRenderer(Context context, Session arSession)
        {
            this.context = context;
            rotationHelper = new DisplayRotationHelper(context);
            ARSession = arSession;
            dataService = App.IoC.Resolve<ARDataService>();
            dataService.UpdateSpeed = true;
            Gesture = new GestureDetector(context, new SimpleTapGestureDetector
            {
                SingleTapUpHandler = (MotionEvent e) =>
                {
                    onSingleTap(e);
                    return true;
                },
                DownHandler = (MotionEvent e) => true
            });
            FrameDrawn += () => framerate.FrameDone();
            framerate.FramerateUpdated += r => dataService.Framerate = r;
        }
        private void onSingleTap(MotionEvent e)
        {
            try
            {
                //waitingEvent = e;
                Func<float[], System.Numerics.Vector3> f = (d) => new System.Numerics.Vector3(d[0], d[1], d[2]);
                //Frame frame = ARSession.Update();
                //var hits = frame.HitTest(e);
                //foreach (var hit in hits)
                //{
                //    var trackable = hit.Trackable;
                //    dataService.XAxis = f(hit.HitPose.GetXAxis());
                //    dataService.YAxis = f(hit.HitPose.GetYAxis());
                //    dataService.ZAxis = f(hit.HitPose.GetZAxis());
                //}
                //dataService.TopTag = "Total hits: " + hits.Count;

            }
            catch (System.Exception) { }
            dataService.Press = new System.Numerics.Vector2(e.RawX, e.RawY);
        }
        public void OnSurfaceCreated(kes.IGL10 gl, Javax.Microedition.Khronos.Egl.EGLConfig config)
        {
            CompassMonitor.StartTracking();
            OrientationMonitor.StartTracking();

            // Set the background color.
            GLES30.GlClearColor(0.5f, 0.5f, 0.5f, 0.5f);
            // Create the texture and pass it to ARCore session to be filled during update().
            mBackgroundRenderer.CreateOnGlThread(/*context=*/context);
            if (this.ARSession != null)
                ARSession.SetCameraTextureName(mBackgroundRenderer.TextureId);

            try
            {
                mVirtualObject.CreateOnGlThread(/*context=*/context, "aircraft.obj", "aircraft.png");
                mVirtualObject.setMaterialProperties(0.0f, 3.5f, 1.0f, 6.0f);

                //mVirtualObjectShadow.CreateOnGlThread(/*context=*/this,
                //        "andy_shadow.obj", "andy_shadow.png");
                //mVirtualObjectShadow.SetBlendMode(ObjectRenderer.BlendMode.Shadow);
                //mVirtualObjectShadow.setMaterialProperties(1.0f, 0.0f, 0.0f, 1.0f);
            }
            catch (Java.IO.IOException e)
            {
                Log.Error("", "Failed to read obj file");
            }

            cloudRenderer.CreateOnGlThread(context);

        }
        public void OnSurfaceChanged(kes.IGL10 gl, int width, int height)
        {
            GlViewport(0, 0, width, height);
            rotationHelper.OnSurfaceChanged(width, height);

            float ratio = (float)width / height;

            float[] translate = new float[16];
            Matrix.SetIdentityM(translate, 0);
            Matrix.TranslateM(translate, 0, 1f, 2f, 3f);
            
            var rotate = new float[16];
            Matrix.SetIdentityM(rotate, 0);
            Matrix.RotateM(rotate, 0, 30, 0, 0, 1);


            // this projection matrix is applied to object coordinates
            // in the onDrawFrame() method
            Matrix.FrustumM(projectionMatrix, 0, -ratio, ratio, -1, 1, 1, 3);

        }

        Vector3 eye = new Vector3(0, 0, 1);
        Vector3 center = new Vector3(0, 0, 0);
        float angle = 45;
        float distance = 2;
        bool move = false;
        static Stopwatch sw = Stopwatch.StartNew();
        List<Anchor> anchors = new List<Anchor>();
        List<Triangle> triangles = new List<Triangle>();
        public void OnDrawFrame(kes.IGL10 gl)
        {
            FrameDrawn?.Invoke();
            if (worldCenter == null) 
            {
                var t1 = new Triangle();
                t1.SetVertices(
                    new Vector3(100f, 20f, 0f),
                    new Vector3(100f, 0f, - 10f),
                    new Vector3(100f, 0f, + 10f)
                    );
                //fixedTriangles.Add(t1); 
                t1 = new Triangle();
                t1.SetVertices(
                    new Vector3(200f, 20f, 0f),
                    new Vector3(200f, 0f, -10f),
                    new Vector3(200f, 0f, +10f)
                    );
                //fixedTriangles.Add(t1);

                worldCenter = new CoordinateCenter();
                worldCenter.SetLength(100f);

                cameraCenter = new CameraCoordinateCenter();
                //image = new TextureRenderer();
                //testTarget = new TestTarget();
                var air = pilot.Aircraft;
                float airWidth = air.ModelWidth * air.ModelScale;
                var circle = new Circle(10);
                circle.SetColor(Color.LightGray);
                circle.SetRadius(airWidth * 1.5f);
                circle.SetOrientation(new AVector3(0, 0, -5), new AVector3(0, 0, -1));
                circles.Add(new CollisionCircle(air, circle, Color.Red));

                circle = new Circle(10);
                circle.SetColor(Color.LightGray);
                circle.SetRadius(airWidth * 1.8f);
                circle.SetOrientation(new AVector3(-5, 0, 0), new AVector3(1, 0, -1));
                circles.Add(new CollisionCircle(air, circle, Color.Blue));
                
                circle = new Circle(10);
                circle.SetColor(Color.LightGray);
                circle.SetRadius(airWidth * 1.8f);
                circle.SetOrientation(new AVector3(5, 0, 0), new AVector3(-1, 0, -1));
                circles.Add(new CollisionCircle(air, circle, Color.Blue));
                
                circle = new Circle(10);
                circle.SetColor(Color.LightGray); 
                circle.SetRadius(airWidth * 1.8f);
                circle.SetOrientation(new AVector3(0, 0, 5), new AVector3(1, 0, 0));
                circles.Add(new CollisionCircle(air, circle, Color.Orange));

                
                
            }
            GlClear(GlColorBufferBit | GlDepthBufferBit);
            rotationHelper.UpdateSessionIfNeeded(ARSession);
            try
            {
                //if (move)
                //    angle++;
                //double angleRad = angle * Math.PI / 180;
                //float newX = (float)Math.Cos(angleRad);
                //float newZ = (float)Math.Sin(angleRad);
                //newX *= distance;
                //newZ *= distance;
                //eye = new Vector3(newX, eye.Y, newZ);
                //// Set the camera position (View matrix)
                //Matrix.SetLookAtM(viewMatrix, 0, eye.X, eye.Y, eye.Z, center.X, center.Y, center.Z, 0f, 1.0f, 0.0f);
                //
                //Matrix.MultiplyMM(vPMatrix, 0, projectionMatrix, 0, viewMatrix, 0);
                
                
                StringBuilder builder = new StringBuilder();
                try
                { 
                    Frame frame = ARSession.Update();
                    Camera camera = frame.Camera;
                    mBackgroundRenderer.Draw(frame);
                    camera.GetProjectionMatrix(projectionMatrix, 0, 0.1f, 500.0f);
                    camera.GetViewMatrix(viewMatrix, 0);
                    
                    Matrix.MultiplyMM(vPMatrix, 0, projectionMatrix, 0, viewMatrix, 0);
                    dataService.IsTracking = camera.TrackingState == TrackingState.Tracking;
                    dataService.TrackingReason = camera.TrackingFailureReason.ToString();

                    testTarget?.Refresh(camera.TrackingState, camera.DisplayOrientedPose);

                    if (camera.TrackingState == TrackingState.Tracking
                        //&& sw.ElapsedMilliseconds > 500
                        && triangles.Count == 0
                        )
                    {
                        foreach (var hit in frame.HitTest(400, 900))
                        {
                            var trackable = hit.Trackable;
                            var anchor = trackable.CreateAnchor(hit.HitPose);
                            float[] matrix = new float[16];
                            anchor.Pose.ToMatrix(matrix, 0);
                            Triangle t = new Triangle();
                            Vector3 top = new Vector3(0, 0.5f, 0);
                            Vector3 left = new Vector3(-0.4f, 0, 0);
                            Vector3 right = new Vector3(0, 0, 0.4f);
                            t.SetVertices(top, left, right);

                            anchors.Add(anchor);
                            triangles.Add(t);
                            
                            dataService.Camera2 = anchor.Pose.GetTranslation().GetVector(0);

                            t.RotationAngleDeg = new Random(DateTime.Now.Millisecond).Next(0, 10);
                            break;
                        }
                        //sw.Restart();
                        // Visualize tracked points.
                    }
                    var pointCloud = frame.AcquirePointCloud();
                    cloudRenderer.Update(pointCloud);
                    cloudRenderer.Draw(camera.DisplayOrientedPose, viewMatrix, projectionMatrix);
                    pointCloud.Release();
                    var point = camera.DisplayOrientedPose.TransformPoint(new float[] { 0f, 0f, 0f });
                    dataService.Camera1 = point.GetVector(0);// FloatToVector(point);
                    builder.Append("Camera pose axes:\n");
                    //point = camera.Pose.TransformPoint(camera.Pose.GetTranslation());
                    //dataService.Camera2 = FloatToVector(point);
                    float[] temp = new float[] { 1f, 0f, 0f };
                    // Pose can return coordinate system rotated n*90 degree around z axis.
                    // DisplayOrientedPose matches to OpenGL system (x-right,y-top,z-towards viewer).
                    var pose = camera.DisplayOrientedPose;
                    var xAxisD = pose.GetXAxis();
                    var yAxisD = pose.GetYAxis();
                    var zAxisD = pose.GetZAxis();
                    pose = camera.Pose;
                    var xAxis = pose.GetXAxis();
                    var yAxis = pose.GetYAxis();
                    var zAxis = pose.GetZAxis();
                    builder.Append("x axis: ");
                    builder.Append(MyVector.VectorToString(xAxisD));
                    builder.AppendLine($" ({MyVector.VectorToString(xAxis)})");
                    builder.Append("y axis: ");
                    builder.Append(MyVector.VectorToString(yAxisD));
                    builder.AppendLine($" ({MyVector.VectorToString(yAxis)})");
                    builder.Append("z axis: ");
                    builder.Append(MyVector.VectorToString(zAxisD));
                    builder.AppendLine($" ({MyVector.VectorToString(zAxis)})");
                    xAxisD = camera.Pose.GetTransformedAxis(0, 1);
                    var localPoint = new float[] { 0f, 0f, 0f };
                    var tpt = camera.Pose.TransformPoint(localPoint);
                    var v = camera.Pose.GetTranslation();
                    builder.AppendLine("Camera translation: ");
                    builder.AppendLine(MyVector.VectorToString(v));
                    builder.AppendLine("Camera view matrix: ");
                    builder.AppendLine(viewMatrix.MatrixString());
                    builder.AppendLine($"Heading: {CompassMonitor.AngleDeg.ToString("0.00")}");
                    builder.AppendLine($"Orientation: {OrientationMonitor.Reading}");


                    testTarget?.Draw(vPMatrix, camera.DisplayOrientedPose);


                    var lightIntensity = frame.LightEstimate.PixelIntensity;
                    float scaleFactor = 0.5f;
                    //for (int i = 0; i < anchors.Count; i++)
                    //{
                    //    var ps = anchors[i].Pose;
                    //    float[] mAnchorMatrix = new float[16];
                    //    ps.ToMatrix(mAnchorMatrix, 0);
                    //    mVirtualObject.updateModelMatrix(mAnchorMatrix, scaleFactor);
                    //    mVirtualObject.Draw(viewMatrix, projectionMatrix, lightIntensity);
                    //}

                    //var vc = pilot.CurrentDirection;
                    //Matrix.SetLookAtM(pilotM, 0, 0,0,0,vc.X, vc.Y, vc.Z, 0, 1, 0);
                    //float[] pilotM = new float[16];
                    //float[] rotate2 = new float[16];
                    //float[] resultM = new float[16];
                    //Matrix.SetIdentityM(pilotM, 0);
                    //Matrix.SetIdentityM(rotate2, 0);
                    //Matrix.RotateM(pilotM, 0, 45f, 0, 1, 0);
                    //Matrix.RotateM(rotate2, 0, 45f, 0, 0, 1);
                    //Matrix.MultiplyMM(resultM, 0, pilotM, 0, rotate2, 0);
                    if (dataService.ResetAircraft)
                    {
                        pilot.Aircraft.Reset(new AVector3(0, 0, -2));
                        //pilot.Aircraft.SetPosition(new Geometry.AVector3(0, 0, -2));
                        //pilot.Aircraft.SetRoll(0);
                        dataService.ResetAircraft = false;
                    }
                    if (dataService.UpdateSpeed)
                    {
                        pilot.Aircraft.Speed = dataService.Speed * Aircraft.MaxSpeed;
                        dataService.UpdateSpeed = false;
                    }
                    //Matrix.RotateM(pilotM, 0, 45f, pilotM[4], pilotM[5], pilotM[6]);
                    //Matrix.RotateM(pilotM, 0, (float)pilot.BankAngleDeg, 0, 0, -1);
                    //var pos = pilot.CurrentPosition;
                    //Matrix.TranslateM(pilotM, 0, pos.X, pos.Y, pos.Z);
                    scaleFactor = pilot.Aircraft.ModelScale;
                    mVirtualObject.updateModelMatrix(pilot.Aircraft.Transform, scaleFactor);
                    mVirtualObject.Draw(viewMatrix, projectionMatrix, lightIntensity);
                    if (dataService.MoveAircraft)
                        pilot.MakeStep();

                    foreach (var c in circles)
                        c.CheckCollision();

                    var cameraVector = viewMatrix.GetTranslation();
                    var pilotBector = pilot.Aircraft.Position;
                    cameraVector.SetHeight();
                    pilotBector.SetHeight();
                    var distance = cameraVector + pilotBector;
                    // Set XY distance only.

                    // Distance to camera.
                    string relativeDistance = distance.Length.ToString("0.00");
                    // Distance to coordinate center.
                    string absoluteDistance = pilotBector.Length.ToString("0.00");
                    string cameraDistance = cameraVector.Length.ToString("0.00");
                    builder.AppendLine($"Camera to center (XY) {cameraDistance}");
                    builder.AppendLine($"Aircraft (XY):\n\t from center: {absoluteDistance} \n\t from camera: {relativeDistance}");


                }
                catch (System.Exception ex)
                {
                    Log.Error("ARCore draw ", ex.Message);
                    throw;
                }
                dataService.TopTag = builder.ToString();

                // Draw shape
                foreach (var tr in fixedTriangles)
                    tr.Draw(vPMatrix);
                worldCenter?.Draw(vPMatrix);
                cameraCenter?.Draw();
                foreach (var circle in circles)
                    circle.DrawCircle(vPMatrix);

                builder.Clear();
                builder.AppendLine($"Anchors ({anchors.Count})");

                for (int i = 0; i < anchors.Count; i++)
                {
                    var t = triangles[i];
                    builder.AppendLine($"Anchor {i} :");
                    builder.AppendLine("Pose matrix: ");
                    float[] mat = new float[16];
                    var pose = anchors[i].Pose;
                    pose.ToMatrix(mat, 0);
                    t.ApplyRotation(mat);
                    builder.AppendLine(mat.MatrixString());
                    Matrix.MultiplyMM(mat, 0, vPMatrix, 0, mat.ToArray(), 0);
                    triangles[i].Draw(mat);

                    var v = new Vector3(pose.Tx(), 0, pose.Tz()).Length().ToString("0.00");
                    builder.AppendLine($"Distance (XY) from center {v}");
                    builder.AppendLine("___________________________");
                }
                dataService.BottomTag = builder.ToString();

            }
            catch (System.Exception ex)
            {
                // Avoid crashing the application due to unhandled exceptions.
                Log.Error("_", "Exception on the OpenGL thread", ex);
            }

        }
    }
    public class MyVector
    {
        public static Vector3 GetVector(float[] v) => new Vector3(v[0], v[1], v[2]);
        public static float[] GetArray(Vector3 v) => new[] { v.X, v.Y, v.Z };
        public static float[] CrossProduct(float[] v1, float[] v2)
        {
            var vector1 = GetVector(v1);
            var vector2 = GetVector(v2);
            var vector = Vector3.Cross(vector1, vector2);
            return GetArray(vector);
        }
        public static float DotProduct(float[] v1, float[] v2)
        {
            var vector1 = GetVector(v1);
            var vector2 = GetVector(v2);
            var val = Vector3.Dot(vector1, vector2);
            return val;
        }

        public static float[] GetTranslation(float[] matrix)
        {
            return matrix.Skip(12).Take(3).ToArray();
        }

        /// <summary>
        /// Returns Up vector for given matrix.
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        public static float[] GetViewUpVector(float[] matrix)
        {
            var v1 = GetVector(matrix);
            var v2 = GetVector(matrix.Skip(4).ToArray());

            throw new NotImplementedException();
        }
        public static string VectorToString(float[] vector)
        {
            var v = vector;
            Func<float, string> c = (f) => string.Format("{0,5:0.00}", f);
            return $" ({c(v[0])}, {c(v[1])}, {c(v[2])}) ";
        }
        public static string VectorToString(Vector3 v)
        {
            Func<float, string> c = (f) => string.Format("{0,5:0.00}", f);
            return $" ({c(v.X)}, {c(v.Y)}, {c(v.Z)}) ";
        }
    }
}