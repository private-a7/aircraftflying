﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARTest.Droid.Models
{
    /// <summary>
    /// Checks configuration for OpenGL.
    /// </summary>
    public class OpenGLManager
    {
        private static double MIN_OPENGL_VERSION = 3.0;
        public static bool CheckIsSupportedDeviceOrFinish(Activity activity)
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.N)
            {
                Toast.MakeText(activity, "Required Android N or later", ToastLength.Long).Show();

                activity.Finish();

                return false;
            }

            // OpenGL in emulator by default is 2.0, unrelated to what GPU on computer supports.
            // To change this value open Emulator, More(...)/Settings/Advanced and there check settings
            // ("Desktop native OpenGL" and "Renderer maximum (up to OpenGL ES 3.1)").
            // More explanation on link: 
            // https://github.com/google-ar/sceneform-android-sdk/issues/220
            var openglString = ((ActivityManager)activity.GetSystemService(Context.ActivityService)).DeviceConfigurationInfo.GlEsVersion;


            if (Double.Parse(openglString) < MIN_OPENGL_VERSION)
            {
                Toast.MakeText(activity, "Required OpenGL ES 3.0 or later", ToastLength.Long).Show();

                return false;
            }

            return true;

        }
    }
}