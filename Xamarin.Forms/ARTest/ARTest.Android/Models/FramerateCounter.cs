﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace ARTest.Droid.Models
{

    public class FramerateCounter : IDisposable
    {
        public event Action<int> FramerateUpdated;

        private Stopwatch sw = Stopwatch.StartNew();
        private int counter = 0;
        public void FrameDone()
        {
            if (sw.ElapsedMilliseconds <= 1000)
                counter++;
            else
            {
                FramerateUpdated?.Invoke(counter);
                counter = 0;
                sw.Restart();
            }
        }

        public void Dispose()
        {
            sw.Stop();
            sw = null;
        }
    }
}