﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Google.AR.Core;
using Google.AR.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARTest.Droid.Models
{
    /// <summary>
    /// Manages starting of ARCore.
    /// </summary>
    public class ARCoreManager
    {
        /// <summary>
        /// After calling Initialize if this is false, check ErrorMessage.
        /// </summary>
        public bool IsRunning => Session != null;
        /// <summary>
        /// Is ARCore supported on device (doesn't mean ARCore is running).
        /// </summary>
        public bool IsSupported { get; private set; } = false;

        public Session Session { get; private set; }

        /// <summary>
        /// Message describing error.
        /// </summary>
        public string ErrorMessage { get; private set; }
        /// <summary>
        /// In case of initialization error, this is exception.
        /// </summary>
        public Java.Lang.Exception Exception { get; private set; }

        private bool isInitialized = false;
        public ARCoreManager()
        {

        }

        /// <summary>
        /// Can be invoked only once. Each next invoke ignores it.
        /// </summary>
        /// <param name="a"></param>
        public void Initialize(Activity a)
        {
            if (isInitialized)
                return;
            isInitialized = true;
            tryCreateSession(a);
            if (IsRunning)
                return;
            // Create default config, check is supported, create session from that config.
            var config = new Google.AR.Core.Config(Session);
#pragma warning disable CS0618 // Type or member is obsolete
            if (!Session.IsSupported(config))
#pragma warning restore CS0618 // Type or member is obsolete
            {
                ErrorMessage = "This device does not support AR";
                Toast.MakeText(a, ErrorMessage, ToastLength.Long).Show();
                IsSupported = false;
                //Finish();
                return;
            }
            Session.Configure(config);
        }
        /// <summary>
        /// Returns message if failed.
        /// </summary>
        /// <returns></returns>
        private bool tryCreateSession(Activity a)
        {
            string message = null;
            IsSupported = true;
            try
            {
                Session = new Session(/*context=*/a);
                //int cameraId = 0;
                //Session.CameraConfig = Session.SupportedCameraConfigs[cameraId];
                return true;
            }
            catch (UnavailableArcoreNotInstalledException e)
            {
                message = "Please install ARCore";
                Exception = e;
            }
            catch (UnavailableApkTooOldException e)
            {
                message = "Please update ARCore";
                Exception = e;
            }
            catch (UnavailableSdkTooOldException e)
            {
                message = "Please update this app";
                Exception = e;
            }
            catch (Java.Lang.Exception e)
            {
                IsSupported = false;
                message = "This device does not support AR";
                Exception = e;
            }

            Toast.MakeText(a, message, ToastLength.Long).Show();
            ErrorMessage = message;
            return false;
        }
    }
}