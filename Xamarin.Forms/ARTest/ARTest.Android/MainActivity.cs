﻿#define USE_ARCORE
using System;
using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Google.AR.Core;
using Android.Content;
using ARTest.Services;
using ARTest.Droid.Models;
using Google.AR.Core.Exceptions;
using ARTest.Droid.Models.OpenGLs;
using v4 = Android.Support.V4.App;
using static Android.OS.PowerManager;

namespace ARTest.Droid
{
    [Activity(Label = "ARTest", Icon = "@mipmap/icon", Theme = "@style/MainTheme", 
        MainLauncher = true, 
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation 
                | ConfigChanges.UiMode | ConfigChanges.ScreenLayout 
                | ConfigChanges.SmallestScreenSize),
        ]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        public ARCoreManager ARManager { get; } = new ARCoreManager();

        //private ModelRenderable andyRenderable;

        private WakeLock wakeLock;

        protected async override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);

            //check to see if the device supports SceneForm
            if (!OpenGLManager.CheckIsSupportedDeviceOrFinish(this))
                return;

            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            var app = App.Create();
            await app.CheckPermissions();
            LoadApplication(app);
#if USE_ARCORE
            ARManager.Initialize(this);
            if (!ARManager.IsRunning)
                return;
#endif                  

            // This is needed, because otherwise null is returned.
            //SupportFragmentManager.ExecutePendingTransactions();
            //arFragment = (GLFragment)SupportFragmentManager.FindFragmentByTag("ar_fragment");

            //var view = App.Current.MainPage.FindByName("arView") as ArCoreViewRenderer;
            
            //ArView.SetOnTouchListener(this);
            
            //set the content
            //SetContentView(Resource.Layout.ARLayout);

            //set the fragment
            //arFragment = (ArFragment)SupportFragmentManager.FindFragmentById(Resource.Id.ux_fragment);

            //load and build the model
            //ModelRenderable.InvokeBuilder().SetSource(this, Resource.Raw.andy).Build(((renderable) =>
            //{
            //    andyRenderable = renderable;
            //
            //}));

            //add the event handler
            //arFragment.TapArPlane += OnTapArPlane;
        }
        protected override void OnResume()
        {
            base.OnResume();
            if (ARManager.IsRunning)
                ARManager.Session.Resume();
            //PowerManager pM = (PowerManager)this.GetSystemService(PowerService);
            //wakeLock = pM.NewWakeLock(WakeLockFlags.Full, "Flight lock");
            //wakeLock.Acquire();
        }
        protected override void OnPause()
        {
            base.OnPause();
            //wakeLock?.Release();
            //if (ARManager.IsRunning)
                //ARManager.Session.Pause();
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        /* private void OnTapArPlane(object sender, BaseArFragment.TapArPlaneEventArgs e)
         {

             //if (andyRenderable == null)
               //  return;

             // Create the Anchor.
             Anchor anchor = e.HitResult.CreateAnchor();
             AnchorNode anchorNode = new AnchorNode(anchor);
             //anchorNode.SetParent(arFragment.ArSceneView.Scene);

             // Create the transformable andy and add it to the anchor.
             //TransformableNode andy = new TransformableNode(arFragment.TransformationSystem);
             //andy.SetParent(anchorNode);
             //andy.Renderable = andyRenderable;
             //andy.Select();

         }*/

    }
}